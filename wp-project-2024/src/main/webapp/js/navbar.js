$(document).ready(function () {
    $("#navbar").load("navbar.html", function() {
        setUpNavbar();
    });
});

function checkUser() {
    $.get({
        url: '/wp-project-2024/rest/status', success: function (data) {
            if(data.username == null) {
                $('#loginBtn').show();
                $('#registrationBtn').show();
            } else {
                $('#logoutBtn').show();
            }
            if(data.role == 'ADMINISTRATOR') {
                $('#addFactoryBtn').show();
                $('#unapprovedCommentsBtn').show();
                $('#profileBtn').show();
                $('#usersBtn').show();
            }
            if(data.role == 'MANAGER') {
                $('#workersBtn').show();
                $('#chocolatesBtn').show();
                $('#ordersBtn').show();
                $('#unapprovedCommentsBtn').show();
                $('#profileBtn').show();
            }
            if(data.role == 'WORKER') {
                $('#chocolateStockBtn').show();
                $('#profileBtn').show();
            }
            if(data.role == 'BUYER') {
                $('#buyerProfileBtn').show();
                $('#rateFactoriesBtn').show();
                $('#profileBtn').show();
            }
        }
    });
}

function logOut() {
    $.post({
        url: '/wp-project-2024/rest/logout',
        contentType : 'application/json', success : function() {
            alert('You have been logged out.');
            window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
        },
        error : function(message) {
            alert(message.responseText);
        }
    });
}

function setUpNavbar() {
    $('#loginBtn').hide();
    $('#registrationBtn').hide();
    $('#logoutBtn').hide();
    $('#addFactoryBtn').hide();
    $('#chocolatesBtn').hide();
    $('#workersBtn').hide();
    $('#chocolateStockBtn').hide();
    $('#buyerProfileBtn').hide();
    $('#ordersBtn').hide();
    $('#rateFactoriesBtn').hide();
    $('#unapprovedCommentsBtn').hide();
    $('#profileBtn').hide();
    $('#usersBtn').hide();

    $('#logoutBtn').click(function () {
        logOut();
    });

    checkUser();
}
