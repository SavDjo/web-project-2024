let factories = [];
var user;
var selectedFactory;


$(document).ready(function () {

    $('#ratingForm').submit(
        rateFactory());

    $.get({
        url: '/wp-project-2024/rest/status',
        success: function(data) {
            user = data;

            $.get({
                url: '/wp-project-2024/rest/factories/user/' + data.username,
                success: function(data) {
                    factories.push(...data);
                    var tableBody = $('#tableBody');
                    data.forEach(function(factory) {
                        var row = $('<tr>');
                        row.append($('<td>').append($('<img>').attr('src', factory.logo).attr('width', '50').attr('height', '50')));
                        row.append($('<td>').text(factory.name));
                        row.append($('<td>').text(factory.location.address));
                        row.append($('<td>').text(factory.rating + '/5'));
                        var viewDetailsButton = $('<button>').text('View details');
                        viewDetailsButton.attr('data-toggle', 'modal');
                        viewDetailsButton.attr('data-target', '#factoryModal');
                        viewDetailsButton.click(function() {
                            viewDetails(factory);
                        });
                        row.append($('<td>').append(viewDetailsButton));

                        var rateButton = $('<button>').text('Comment&rate');
                        rateButton.click(function () {
                            createRatingForm(factory.name);
                        });
                        row.append($('<td>').append(rateButton));

                        tableBody.append(row);
                    });
                }
            });
        }
    });



});

function rateFactory() {
    return function (event) {
        event.preventDefault();

        let comment = $('#commentInput').val();
        let rating = $('#ratingInput').val();

        $.ajax({
            url: '/wp-project-2024/rest/comments/',
            type: 'POST',
            data: JSON.stringify({
                author: user.username,
                factory: selectedFactory.name, 
                content: comment, 
                rating: rating, 
                isApproved: false
            }),
            contentType: 'application/json',
            success: function () {
                window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
            }
        });
    }
}

function createRatingForm(factoryName) {
    selectedFactory = factories.find(factory => factory.name === factoryName);

    $('#ratingForm').show();
}

function updateFactoryTable(filteredFactories) {
    var tableBody = $('#tableBody');
    tableBody.empty();

    filteredFactories.forEach(function (factory) {
        var row = $('<tr>');
        row.append($('<td>').append($('<img>').attr('src', factory.logo).attr('width', '50').attr('height', '50')));
        row.append($('<td>').text(factory.name));
        row.append($('<td>').text(factory.location.address));
        row.append($('<td>').text(factory.rating + '/5'));
        row.append($('<td>').append($('<button>').text('View details').click(function () {
            viewDetails(factory);
        })));
        tableBody.append(row);
    });
}

function viewDetails(factory) {

    $('#factoryDetails').empty();

    var factoryDiv = $('<div>');

    factoryDiv.append($('<h2>').text(factory.name));
    factoryDiv.append($('<p>').text('Location: ' + factory.location.address));
    factoryDiv.append($('<p>').text('Opening Time: ' + factory.openingTime));
    factoryDiv.append($('<p>').text('Closing Time: ' + factory.closingTime));
    factoryDiv.append($('<p>').text('Rating: ' + factory.rating));
    factoryDiv.append($('<p>').text('Status: ' + factory.status));

    var chocolateList = $('<ul>');
    factory.chocolates.forEach(function(chocolate) {
        var chocolateItem = $('<li>');
        chocolateItem.append($('<h3>').text(chocolate.name));
        chocolateItem.append($('<p>').text('Price: ' + chocolate.price));
        chocolateItem.append($('<p>').text('Type: ' + chocolate.type));
        chocolateItem.append($('<p>').text('Weight: ' + chocolate.weight));
        chocolateItem.append($('<p>').text('Description: ' + chocolate.description));
        chocolateItem.append($('<p>').text('Status: ' + chocolate.status));
        chocolateItem.append($('<p>').text('Count: ' + chocolate.count));
        chocolateItem.append($('<p>').text('Category: ' + chocolate.category));
        chocolateList.append(chocolateItem);
    });
    factoryDiv.append(chocolateList);

    $('#factoryDetails').append(factoryDiv);

    $('#factoryModal').modal('show');
}

