let users = [];

$(document).ready(function () {

    $.get({
        url: '/wp-project-2024/rest/users',
        success: function (data) {

            data.forEach(function (user) {
                users.push(...data);
                var tableBody = $('#tableBody');
                var row = $('<tr>');
                row.append($('<td>').text(user.username));
                row.append($('<td>').text(user.password));
                row.append($('<td>').text(user.firstName));
                row.append($('<td>').text(user.lastName));
                row.append($('<td>').text(user.gender));
                row.append($('<td>').text(user.dateOfBirth));
                row.append($('<td>').text(user.role));
    
                tableBody.append(row);
            });
        }
    });

    document.getElementById('searchBtn').addEventListener('click', searchUsers);

    const nameCheckbox = document.getElementById('searchByName');
    const nameInput = document.getElementById('nameInputDiv');
    const searchBtn = document.getElementById('searchBtn');

    nameCheckbox.addEventListener('change', function () {
        if (this.checked) {
            nameInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            nameInput.style.display = 'none';
        }
    });

    const lastNameCheckbox = document.getElementById('searchByLastName');
    const lastNameInput = document.getElementById('lastNameInputDiv');

    lastNameCheckbox.addEventListener('change', function () {
        if (this.checked) {
            lastNameInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            lastNameInput.style.display = 'none';
        }
    });

    const usernameCheckbox = document.getElementById('searchByUsername');
    const usernameInput = document.getElementById('usernameInputDiv');

    locationCheckbox.addEventListener('change', function () {
        if (this.checked) {
            usernameInput.style.display = 'block';
            usernameBtn.style.display = 'block';
        } else {
            usernameInput.style.display = 'none';
        }
    });

    const userTypeDropdown = document.getElementById('userType');

    userTypeDropdown.addEventListener('change', () => {
        const selectedStatus = userTypeDropdown.value;

        const filteredUsers = filterByType(selectedType);

        updateUserTable(filteredUsers);
    });

    const userRoleDropdown = document.getElementById('userRole');

    userRoleDropdown.addEventListener('change', () => {
        const selectedRole = userRoleDropdown.value;

        const filteredUsers = filterByUserRole(selectedRole);

        updateUserTable(filteredUsers);
    });

});

function searchFactories() {
    const searchByName = $('#searchByName').prop('checked');
    const searchByLastName = $('#searchByLastName').prop('checked');
    const searchByUsername = $('#searchByUsername').prop('checked');

    const firstName = $('#nameInput').val();
    const lastName = $('#lastNameInput').val();
    const username = $('#usernameInput').val();

    $.get({
        url: '/wp-project-2024/rest/users',
        success: function (data) {
            users = data;

            let filteredUsers = users;
            if (searchByName) {
                filteredUsers = filteredUsers.filter(user => user.firstName.toLowerCase().includes(firstName.toLowerCase()));
            }
            if (searchByLastName) {
                filteredUsers = filteredUsers.filter(user => user.lastName.toLowerCase().includes(lastName.toLowerCase()));
            }
            if (searchByUsername) {
                filteredUsers = filteredUsers.filter(user => filteredUsers.username.toLowerCase().includes(username.toLowerCase()));
            }

            updateUserTable(filteredUsers);
        }
    });
}

function updateUserTable(filteredUsers) {
    var tableBody = $('#tableBody');
    tableBody.empty();

    filteredUsers.forEach(function (user) {
        var row = $('<tr>');
        row.append($('<td>').text(user.username));
        row.append($('<td>').text(user.password));
        row.append($('<td>').text(user.firstName));
        row.append($('<td>').text(user.lastName));
        row.append($('<td>').text(user.gender));
        row.append($('<td>').text(user.dateOfBirth));
        row.append($('<td>').text(user.role));

        tableBody.append(row);
    });
}
