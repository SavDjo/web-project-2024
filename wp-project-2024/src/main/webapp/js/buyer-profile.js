let orders = [];
let nameSortAscending = true;
let priceSortAscending = true;
let dateSortAscending = true;

$(document).ready(function () {

    $.get({
        url: '/wp-project-2024/rest/status',
        success: function(data) {
            orders.push(data.orders);
            var tableBody = $('#tableBody');
            data.orders.forEach(function(order) {
                var row = $('<tr>');
                row.append($('<td>').text(order.factory.name));
                row.append($('<td>').text(order.price));
                row.append($('<td>').text(order.purchaseTime));
                var chocolateNames = order.chocolates.map(chocolate => chocolate.name).join(', ');
                row.append($('<td>').text(chocolateNames));
                row.append($('<td>').text(order.status.toLowerCase()));
                tableBody.append(row);
            });
        }
    });

    document.getElementById('searchBtn').addEventListener('click', searchOrders);

    const nameCheckbox = document.getElementById('searchByName');
    const nameInput = document.getElementById('factoryNameInputDiv');
    const searchBtn = document.getElementById('searchBtn');

    nameCheckbox.addEventListener('change', function () {
        if (this.checked) {
            nameInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            nameInput.style.display = 'none';
        }
    });

    const priceCheckbox = document.getElementById('searchByPrice');
    const fromPriceInput = document.getElementById('fromPriceInputDiv');
    const toPriceInput = document.getElementById('toPriceInputDiv');

    priceCheckbox.addEventListener('change', function () {
        if (this.checked) {
            fromPriceInput.style.display = 'block';
            toPriceInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            fromPriceInput.style.display = 'none';
            toPriceInput.style.display = 'none';
        }
    });

    const dateCheckbox = document.getElementById('searchByDate');
    const fromDateInput = document.getElementById('fromDateInputDiv');
    const toDateInput = document.getElementById('toDateInputDiv');

    dateCheckbox.addEventListener('change', function () {
        if (this.checked) {
            fromDateInput.style.display = 'block';
            toDateInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            fromDateInput.style.display = 'none';
            toDateInput.style.display = 'none';
        }
    });
});

function sortByName() {
    nameSortAscending = !nameSortAscending;

    orders.sort((a, b) => {
        const comparison = a.factory.name.localeCompare(b.factory.name);
        return nameSortAscending ? comparison : -comparison;
    });

    updateOrderTable(orders);
}

function sortByPrice() {
    priceSortAscending = !priceSortAscending;

    orders.sort((a, b) => {
        return priceSortAscending ? a.price - b.price : b.price - a.price;
    });

    updateOrderTable(orders);
}

function sortByDate() {
    dateSortAscending = !dateSortAscending;

    orders.sort((a, b) => {
        const comparison = a.purchaseTime.localeCompare(b.purchaseTime);
        return dateSortAscending ? comparison : -comparison;
    });

    updateOrderTable(orders);
}


function searchOrders() {
    const searchByName = $('#searchByName').prop('checked');
    const searchByPrice = $('#searchByPrice').prop('checked');
    const searchByDate = $('#searchByDate').prop('checked');

    const factoryName = $('#factoryNameInput').val();
    const fromPrice = $('#fromPriceInput').val();
    const toPrice = $('#toPriceInput').val();
    const fromDate = $('#fromDateInput').val();
    const toDate = $('#toDateInput').val();

    $.get({
        url: '/wp-project-2024/rest/status',
        success: function (data) {
            let orders = data.orders;

            let filteredOrders = orders;
            if (searchByName) {
                filteredOrders = filteredOrders.filter(order => order.factory.name.toLowerCase().includes(factoryName.toLowerCase()));
            }
            if (searchByPrice) {
                filteredOrders = filteredOrders.filter(order => order.price >= parseFloat(fromPrice) && order.price <= parseFloat(toPrice));
            }
            if (searchByDate) {
                filteredOrders = filteredOrders.filter(order => {
                    const orderDate = new Date(order.purchaseTime + 'T00:00:00');
                    return orderDate >= new Date(fromDate) && orderDate <= new Date(toDate);
                });
            }

            updateOrderTable(filteredOrders);
        }
    });
}



function updateOrderTable(filteredOrders) {
    var tableBody = $('#tableBody');
    tableBody.empty();

    filteredOrders = filteredOrders.flat();

    filteredOrders.forEach(function (order) {
        var row = $('<tr>');
        row.append($('<td>').text(order.factory.name));
        row.append($('<td>').text(order.price));
        row.append($('<td>').text(order.purchaseTime));
        var chocolateNames = order.chocolates.map(chocolate => chocolate.name).join(', ');
        row.append($('<td>').text(chocolateNames));
        row.append($('<td>').text(order.status.toLowerCase()));
        tableBody.append(row);
    });
}

