let factories = [];
let nameSortAscending = true;
let locationSortAscending = true;
let ratingSortAscending = true;

$(document).ready(function () {

    $.get({
        url: '/wp-project-2024/rest/factories',
        success: function(data) {
            factories.push(...data);
            var tableBody = $('#tableBody');
            data.forEach(function(factory) {
                var row = $('<tr>');
                row.append($('<td>').append($('<img>').attr('src', factory.logo).attr('width', '50').attr('height', '50')));
                row.append($('<td>').text(factory.name));
                row.append($('<td>').text(factory.location.address));
                row.append($('<td>').text(factory.rating + '/5'));
                var viewDetailsButton = $('<button>').text('View details');
                viewDetailsButton.attr('data-toggle', 'modal');
                viewDetailsButton.attr('data-target', '#factoryModal');
                viewDetailsButton.click(function() {
                    viewDetails(factory);
                });

                row.append($('<td>').append(viewDetailsButton));
                tableBody.append(row);
            });
        }
    });

    document.getElementById('searchBtn').addEventListener('click', searchFactories);

    const nameCheckbox = document.getElementById('searchByName');
    const nameInput = document.getElementById('factoryNameInputDiv');
    const searchBtn = document.getElementById('searchBtn');

    nameCheckbox.addEventListener('change', function () {
        if (this.checked) {
            nameInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            nameInput.style.display = 'none';
        }
    });

    const chocolateCheckbox = document.getElementById('searchByChocolateName');
    const chocolateInput = document.getElementById('chocolateNameInputDiv');

    chocolateCheckbox.addEventListener('change', function () {
        if (this.checked) {
            chocolateInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            chocolateInput.style.display = 'none';
        }
    });

    const locationCheckbox = document.getElementById('searchByLocation');
    const locationInput = document.getElementById('locationInputDiv');

    locationCheckbox.addEventListener('change', function () {
        if (this.checked) {
            locationInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            locationInput.style.display = 'none';
        }
    });

    const ratingCheckbox = document.getElementById('searchByRating');
    const ratingInput = document.getElementById('ratingInputDiv');

    ratingCheckbox.addEventListener('change', function () {
        if (this.checked) {
            ratingInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            ratingInput.style.display = 'none';
        }
    });

    const factoryStatusDropdown = document.getElementById('factoryStatus');

    factoryStatusDropdown.addEventListener('change', () => {
        const selectedStatus = factoryStatusDropdown.value;

        const filteredFactories = filterByStatus(selectedStatus);

        updateFactoryTable(filteredFactories);
    });

    const chocolateTypeDropdown = document.getElementById('chocolateType');

    chocolateTypeDropdown.addEventListener('change', () => {
        const selectedStatus = chocolateTypeDropdown.value;

        const filteredFactories = filterByChocolateType(selectedStatus);

        updateFactoryTable(filteredFactories);
    });

    const chocolateCategoryDropdown = document.getElementById('chocolateCategory');

    chocolateCategoryDropdown.addEventListener('change', () => {
        const selectedStatus = chocolateCategoryDropdown.value;

        const filteredFactories = filterByChocolateCategory(selectedStatus);

        updateFactoryTable(filteredFactories);
    });
});

function sortByName() {
    nameSortAscending = !nameSortAscending;

    factories.sort((a, b) => {
        const comparison = a.name.localeCompare(b.name);
        return nameSortAscending ? comparison : -comparison;
    });

    updateFactoryTable(factories);
}

function sortByLocation() {
    locationSortAscending = !locationSortAscending;

    factories.sort((a, b) => {
        const comparison = a.location.address.localeCompare(b.location.address);
        return locationSortAscending ? comparison : -comparison;
    });

    updateFactoryTable(factories);
}

function sortByRating() {
    ratingSortAscending = !ratingSortAscending;

    factories.sort((a, b) => {
        return ratingSortAscending ? a.rating - b.rating : b.rating - a.rating;
    });

    updateFactoryTable(factories);
}

function filterByStatus(status) {
    return factories.filter(factory => factory.status === status);
}

function filterByChocolateType(status) {
    return factories.filter(factory => factory.chocolates.some(chocolate => chocolate.type === status));
}

function filterByChocolateCategory(status) {
    return factories.filter(factory => factory.chocolates.some(chocolate => chocolate.category === status));
}


function searchFactories() {
    const searchByName = $('#searchByName').prop('checked');
    const searchByChocolateName = $('#searchByChocolateName').prop('checked');
    const searchByLocation = $('#searchByLocation').prop('checked');
    const searchByRating = $('#searchByRating').prop('checked');

    const factoryName = $('#factoryNameInput').val();
    const chocolateName = $('#chocolateNameInput').val();
    const location = $('#locationInput').val();
    const rating = parseFloat($('#ratingInput').val());

    $.get({
        url: '/wp-project-2024/rest/factories',
        success: function (data) {
            factories = data;

            let filteredFactories = factories;
            if (searchByName) {
                filteredFactories = filteredFactories.filter(factory => factory.name.toLowerCase().includes(factoryName.toLowerCase()));
            }
            if (searchByChocolateName) {
                filteredFactories = filteredFactories.filter(factory => factory.chocolates.some(chocolate => chocolate.name.toLowerCase().includes(chocolateName.toLowerCase())));
            }
            if (searchByLocation) {
                filteredFactories = filteredFactories.filter(factory => factory.location.address.toLowerCase().includes(location.toLowerCase()));
            }
            if (searchByRating) {
                filteredFactories = filteredFactories.filter(factory => factory.rating >= rating);
            }

            updateFactoryTable(filteredFactories);
        }
    });
}

function updateFactoryTable(filteredFactories) {
    var tableBody = $('#tableBody');
    tableBody.empty();

    filteredFactories.forEach(function (factory) {
        var row = $('<tr>');
        row.append($('<td>').append($('<img>').attr('src', factory.logo).attr('width', '50').attr('height', '50')));
        row.append($('<td>').text(factory.name));
        row.append($('<td>').text(factory.location.address));
        row.append($('<td>').text(factory.rating + '/5'));
        row.append($('<td>').append($('<button>').text('View details').click(function () {
            viewDetails(factory);
        })));
        tableBody.append(row);
    });
}

function viewDetails(factory) {

    $('#factoryDetails').empty();

    var factoryDiv = $('<div>');

    factoryDiv.append($('<h2>').text(factory.name));
    factoryDiv.append($('<p>').text('Location: ' + factory.location.address));
    factoryDiv.append($('<p>').text('Opening Time: ' + factory.openingTime));
    factoryDiv.append($('<p>').text('Closing Time: ' + factory.closingTime));
    factoryDiv.append($('<p>').text('Rating: ' + factory.rating));
    factoryDiv.append($('<p>').text('Status: ' + factory.status));

    var chocolateList = $('<ul>');
    factory.chocolates.forEach(function(chocolate) {
        var chocolateItem = $('<li>');
        chocolateItem.append($('<h3>').text(chocolate.name));
        chocolateItem.append($('<p>').text('Price: ' + chocolate.price));
        chocolateItem.append($('<p>').text('Type: ' + chocolate.type));
        chocolateItem.append($('<p>').text('Weight: ' + chocolate.weight));
        chocolateItem.append($('<p>').text('Description: ' + chocolate.description));
        chocolateItem.append($('<p>').text('Status: ' + chocolate.status));
        chocolateItem.append($('<p>').text('Count: ' + chocolate.count));
        chocolateItem.append($('<p>').text('Category: ' + chocolate.category));
        chocolateList.append(chocolateItem);
    });
    factoryDiv.append(chocolateList);

    let comments = []

    $.get({
        url: '/wp-project-2024/rest/comments/factory/' + factory.name,
        success: function(data) {
            comments.push(...data);
            var commentList = $('<ul>');
            data.forEach(function(comment) {
                var commentItem = $('<li>');
                commentItem.append($('<h3>').text(comment.author));
                commentItem.append($('<p>').text(comment.content));
                commentList.append(commentItem);
            });
            factoryDiv.append(commentList);
        
            $('#factoryDetails').append(factoryDiv);
        
            $('#factoryModal').modal('show');
        }
    });

    var commentList = $('<ul>');
    comments.forEach(function(comment) {
        var commentItem = $('<li>');
        commentItem.append($('<h3>').text(comment.author));
        commentItem.append($('<p>').text(comment.content));
        commentList.append(commentItem);
    });
    factoryDiv.append(commentList);

    $('#factoryDetails').append(factoryDiv);

    $('#factoryModal').modal('show');
}

