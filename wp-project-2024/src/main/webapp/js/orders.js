let orders = [];
var selectedOrder;
let nameSortAscending = true;
let priceSortAscending = true;
let dateSortAscending = true;

$(document).ready(function () {
    $('#rejectionForm').submit(
        rejectOrder());

    $.get({
        url: '/wp-project-2024/rest/managers/orders',
        success: function(data) {
            orders.push(data);
            var tableBody = $('#tableBody');
            data.forEach(function(order) {
                var row = $('<tr>');
                row.append($('<td>').text(order.factory.name));
                row.append($('<td>').text(order.price));
                row.append($('<td>').text(order.purchaseTime));
                var chocolateNames = order.chocolates.map(chocolate => chocolate.name).join(', ');
                row.append($('<td>').text(chocolateNames));
                row.append($('<td>').text(order.status.toLowerCase()));

                var approveButton = $('<button>').text('Approve');
                approveButton.click(function () {
                    approveOrder(order.id);
                });
                row.append($('<td>').append(approveButton));

                var rejectButton = $('<button>').text('Reject');
                rejectButton.click(function () {
                    createRejectionForm(order.id);
                });
                row.append($('<td>').append(rejectButton));

                tableBody.append(row);
            });
        }
    });

    document.getElementById('searchBtn').addEventListener('click', searchOrders);

    const priceCheckbox = document.getElementById('searchByPrice');
    const fromPriceInput = document.getElementById('fromPriceInputDiv');
    const toPriceInput = document.getElementById('toPriceInputDiv');

    priceCheckbox.addEventListener('change', function () {
        if (this.checked) {
            fromPriceInput.style.display = 'block';
            toPriceInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            fromPriceInput.style.display = 'none';
            toPriceInput.style.display = 'none';
        }
    });

    const dateCheckbox = document.getElementById('searchByDate');
    const fromDateInput = document.getElementById('fromDateInputDiv');
    const toDateInput = document.getElementById('toDateInputDiv');

    dateCheckbox.addEventListener('change', function () {
        if (this.checked) {
            fromDateInput.style.display = 'block';
            toDateInput.style.display = 'block';
            searchBtn.style.display = 'block';
        } else {
            fromDateInput.style.display = 'none';
            toDateInput.style.display = 'none';
        }
    });
});

function createRejectionForm(orderId) {
    selectedOrder = orders.find(order => order.id === orderId);

    $('#rejectionForm').show();
}

function rejectOrder() {
    return function (event) {
        event.preventDefault();

        let formData = new FormData();
        formData.append('id', selectedOrder.id);
        formData.append('rejectionReason', $('#inputRejection').val());
    
        $.ajax({
            url: '/wp-project-2024/rest/managers/orders/reject',
            type: 'PUT',
            data: formData,
            processData: false,
            contentType: false,
            success: function () {
                window.location.href = "http://localhost:8080/wp-project-2024/orders.html";
            }
        });
    }
}

function approveOrder(orderId) {
    $.ajax({
        url: '/wp-project-2024/rest/managers/orders/approve/' + orderId,
        type: 'PUT',
        processData: false,
        contentType: false,
        success: function () {
            window.location.href = "http://localhost:8080/wp-project-2024/orders.html";
        }
    });

}

function sortByPrice() {
    priceSortAscending = !priceSortAscending;

    orders.sort((a, b) => {
        return priceSortAscending ? a.price - b.price : b.price - a.price;
    });

    updateOrderTable(orders);
}

function sortByDate() {
    dateSortAscending = !dateSortAscending;

    orders.sort((a, b) => {
        const comparison = a.purchaseTime.localeCompare(b.purchaseTime);
        return dateSortAscending ? comparison : -comparison;
    });

    updateOrderTable(orders);
}


function searchOrders() {
    const searchByPrice = $('#searchByPrice').prop('checked');
    const searchByDate = $('#searchByDate').prop('checked');

    const factoryName = $('#factoryNameInput').val();
    const fromPrice = $('#fromPriceInput').val();
    const toPrice = $('#toPriceInput').val();
    const fromDate = $('#fromDateInput').val();
    const toDate = $('#toDateInput').val();

    $.get({
        url: '/wp-project-2024/rest/managers/orders',
        success: function (data) {
            let orders = data;

            let filteredOrders = orders;

            if (searchByPrice) {
                filteredOrders = filteredOrders.filter(order => order.price >= parseFloat(fromPrice) && order.price <= parseFloat(toPrice));
            }
            if (searchByDate) {
                filteredOrders = filteredOrders.filter(order => {
                    const orderDate = new Date(order.purchaseTime + 'T00:00:00');
                    return orderDate >= new Date(fromDate) && orderDate <= new Date(toDate);
                });
            }

            updateOrderTable(filteredOrders);
        }
    });
}



function updateOrderTable(filteredOrders) {
    var tableBody = $('#tableBody');
    tableBody.empty();

    filteredOrders = filteredOrders.flat();

    filteredOrders.forEach(function (order) {
        var row = $('<tr>');
        row.append($('<td>').text(order.factory.name));
        row.append($('<td>').text(order.price));
        row.append($('<td>').text(order.purchaseTime));
        var chocolateNames = order.chocolates.map(chocolate => chocolate.name).join(', ');
        row.append($('<td>').text(chocolateNames));
        row.append($('<td>').text(order.status.toLowerCase()));

        var approveButton = $('<button>').text('Approve');
        approveButton.click(function () {
            approveOrder(order.id);
        });
        row.append($('<td>').append(approveButton));

        var rejectButton = $('<button>').text('Reject');
        rejectButton.click(function () {
            createRejectionForm(order.id);
        });
        row.append($('<td>').append(rejectButton));

        tableBody.append(row);
    });
}

