let comments = [];


$(document).ready(function () {

    $.get({
        url: '/wp-project-2024/rest/comments/not-approved',
        success: function (data) {
            comments.push(...data);
            var tableBody = $('#tableBody');
            data.forEach(function (comment) {
                var row = $('<tr>');
                row.append($('<td>').text(comment.author));
                row.append($('<td>').text(comment.factory));
                row.append($('<td>').text(comment.content));
                var approveButton = $('<button>').text('Approve');
                approveButton.click(function () {
                    approveComment(comment.id);
                });
                row.append($('<td>').append(approveButton));

                var rejectButton = $('<button>').text('Reject');
                rejectButton.click(function () {
                    rejectComment(comment.id);
                });
                row.append($('<td>').append(rejectButton));

                tableBody.append(row);
            });
        }
    });
});

function approveComment(commentId) {
    $.ajax({
        url: '/wp-project-2024/rest/comments/approve/' + commentId,
        type: 'PUT',
        processData: false,
        contentType: false,
        success: function () {
            window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
        }
    });
}

function rejectComment(commentId) {
    $.ajax({
        url: '/wp-project-2024/rest/comments/reject/' + commentId,
        type: 'PUT',
        processData: false,
        contentType: false,
        success: function () {
            window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
        }
    });
}

