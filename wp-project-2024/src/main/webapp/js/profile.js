var user;

$(document).ready(function () {

    $('#updateForm').submit(
        updateUser());

    $.get({
        url: '/wp-project-2024/rest/status',
        success: function (data) {
            user = data;
            var tableBody = $('#tableBody');
            var row = $('<tr>');
            row.append($('<td>').text(data.username));
            row.append($('<td>').text(data.password));
            row.append($('<td>').text(data.firstName));
            row.append($('<td>').text(data.lastName));
            row.append($('<td>').text(data.gender));
            row.append($('<td>').text(data.dateOfBirth));


            var updateButton = $('<button>').text('Update');
            updateButton.click(function () {
                createUpdateForm(user);
            });
            row.append($('<td>').append(updateButton));

            tableBody.append(row);
        }
    });
});

function createUpdateForm(user) {
    $('#inputUsername').val(user.username);
    $('#inputPassword').val(user.password);
    $('#inputFirstName').val(user.firstName);
    $('#inputLastName').val(user.lastName);
    $('#inputGender').val(user.gender);
    $('#inputDateOfBirth').val(user.dateOfBirth);

    $('#updateForm').show();
}

function updateUser() {
    return function (event) {
        event.preventDefault();

        let username = $('#inputUsername').val();
        let password = $('#inputPassword').val();
        let firstName = $('#inputFirstName').val();
        let lastName = $('#inputLastName').val();
        let gender = $('#gender').val();
        let dateOfBirth = $('#dateOfBirth').val();

        $.post({
            url: '/wp-project-2024/rest/update', data: JSON.stringify({
                username: username, password: password, firstName: firstName, lastName: lastName, gender: gender,
                dateOfBirth: dateOfBirth, role: user.role
            }),
            contentType: 'application/json', success: function () {
                window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
            }
        });
    }
}