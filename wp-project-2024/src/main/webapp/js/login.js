$(document).ready(function () {
    $('#form-login').submit(function () {
        logIn();
    });
});

function logIn() {
	let username = $('#inputUsername').val();
    let password = $('#inputPassword').val();

    $.post({
        url: '/wp-project-2024/rest/login', data: JSON.stringify({username: username, password: password}),
        contentType : 'application/json', success : function() {
            alert('You are logged in.');
            window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
        },
        error : function(message) {
            alert(message.responseText);
        }
    });
}