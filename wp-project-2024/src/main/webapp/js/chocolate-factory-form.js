$(document).ready(function () {
    loadManagers();
    $('#form-factory').submit(
        addFactory());
});

function loadManagers() {
    $.ajax({
        url: '/wp-project-2024/rest/managers/',
        type: 'GET',
        dataType: 'json',
        success: function (managers) {
            var availableManagers = managers.filter(function (manager) {
                return !manager.factory;
            });

            if (availableManagers.length > 0) {
                $('#noManagersMessage').hide();
                availableManagers.forEach(function (manager) {
                    $('#inputManager').append($('<option>', {
                        value: manager.id,
                        text: manager.firstName + ' ' + manager.lastName
                    }));
                });
            } else {
                $('#inputManager').prop('disabled', true);
                $('#noManagersMessage').show();
                addManagerForm();
            }
        }
    });
}

function addManagerForm() {
    var managerForm = $('<div class="manager-form">');

    var usernameLabel = $('<label for="inputManagerUsername">Username</label>');
    var usernameInput = $('<input type="text" id="inputManagerUsername" class="form-control" placeholder="Username" required>');
    managerForm.append(usernameLabel, usernameInput);

    var passwordLabel = $('<label for="inputManagerPassword">Password</label>');
    var passwordInput = $('<input type="text" id="inputManagerPassword" class="form-control" placeholder="Password" required>');
    managerForm.append(passwordLabel, passwordInput);

    var firstNameLabel = $('<label for="inputManagerFirstName">First Name</label>');
    var firstNameInput = $('<input type="text" id="inputManagerFirstName" class="form-control" placeholder="First Name" required>');
    managerForm.append(firstNameLabel, firstNameInput);

    var lastNameLabel = $('<label for="inputManagerLastName">Last Name</label>');
    var lastNameInput = $('<input type="text" id="inputManagerLastName" class="form-control" placeholder="Last Name" required>');
    managerForm.append(lastNameLabel, lastNameInput);

    var genderLabel = $('<label for="inputManagerGender">Gender</label>');
    var genderInput = $('<select id="inputManagerGender" class="form-control" required>' +
        '<option value="MALE">Male</option>' +
        '<option value="FEMALE">Female</option>' +
        '</select>');
    managerForm.append(genderLabel, genderInput);

    var dobLabel = $('<label for="inputManagerDOB">Date of Birth</label>');
    var dobInput = $('<input type="date" id="inputManagerDOB" class="form-control" required>');
    managerForm.append(dobLabel, dobInput);

    $('#managerFormContainer').append(managerForm);
}

function addFactory() {
    return function (event) {
        event.preventDefault();

        let formData = new FormData();
        formData.append('name', $('#inputName').val());
        formData.append('openingTime', $('#inputOpeningTime').val());
        formData.append('closingTime', $('#inputClosingTime').val());
        formData.append('logo', $('#inputLogo')[0].files[0]);
        formData.append('longitude', $('#inputLongitude').val());
        formData.append('latitude', $('#inputLatitude').val());
        formData.append('address', $('#inputAddress').val());

        var selectedManagerId = $('#inputManager').val();
        if (selectedManagerId) {
            formData.append('manager', selectedManagerId);

            $.ajax({
                url: '/wp-project-2024/rest/factories/',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function () {
                    window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
                }
            });
        } else {
            formData.append('username', $('#inputManagerUsername').val());
            formData.append('password', $('#inputManagerPassword').val());
            formData.append('firstName', $('#inputManagerFirstName').val());
            formData.append('lastName', $('#inputManagerLastName').val());
            formData.append('gender', $('#inputManagerGender').val());
            formData.append('dateOfBirth', $('#inputManagerDOB').val());

            $.ajax({
                url: '/wp-project-2024/rest/factories/new-manager',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function () {
                    window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
                }
            });
        }
    }
}