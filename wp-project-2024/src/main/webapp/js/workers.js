let workers = [];
var factory;

$(document).ready(function () {
    $("#registrationButton").click(function () {
        $("#workerForm").toggle();
    });

    $('#workerForm').submit(
        register());

    $.get({
        url: '/wp-project-2024/rest/managers/factory',
        success: function (data) {
            factory = data;
        }
    });

    $.get({
        url: '/wp-project-2024/rest/workers',
        success: function (data) {
            workers.push(...data);
            var tableBody = $('#tableBody');
            data.forEach(function (worker) {
                var row = $('<tr>');
                row.append($('<td>').text(worker.firstName + ' ' + worker.lastName));
                var deleteButton = $('<button>').text('Delete worker');
                deleteButton.click(function () {
                    deleteWorker(worker.id);
                });

                row.append($('<td>').append(deleteButton));

                tableBody.append(row);
            });
        }
    });

});

function deleteWorker(id) {
    $.ajax({
        url: `/wp-project-2024/rest/workers/${id}`,
        type: 'DELETE',
        success: function (response) {
            console.log('Deletion successful', response);
            window.location.href = "http://localhost:8080/wp-project-2024/workers.html";
        },
        error: function (xhr, status, error) {
            console.error('Error occurred during deletion', error);
        }
    });


}

function register() {
    return function (event) {
        event.preventDefault();


        let username = $('#inputUsername').val();
        let password = $('#inputPassword').val();
        let firstName = $('#inputFirstName').val();
        let lastName = $('#inputLastName').val();
        let gender = $('#gender').val();
        let dateOfBirth = $('#dateOfBirth').val();

        $.post({
            url: '/wp-project-2024/rest/workers/', data: JSON.stringify({
                username: username, password: password, firstName: firstName, lastName: lastName, gender: gender,
                dateOfBirth: dateOfBirth, role: 'WORKER', factory: null
            }),
            contentType: 'application/json', success: function () {
                $.ajax({
                    type: 'PUT',
                    url: '/wp-project-2024/rest/workers/factory/' + username,
                    data: JSON.stringify(factory),
                    contentType: 'application/json',
                    success: function () {
                        window.location.href = "http://localhost:8080/wp-project-2024/workers.html";
                    },
                    error: function () {
                        console.error('Error occured setting worker factory.');
                    }
                });
            }
        });
    }
}


