let chocolates = [];

$(document).ready(function () {
    $("#registrationButton").click(function () {
        $("#chocolateForm").toggle();
    });

    $('#chocolateForm').submit(
        register());

    $('#chocolateUpdateForm').submit(
        updateChocolate());

    $.get({
        url: '/wp-project-2024/rest/chocolates',
        success: function (data) {
            chocolates.push(...data);
            var tableBody = $('#tableBody');
            data.forEach(function (chocolate) {
                var row = $('<tr>');
                row.append($('<td>').append($('<img>').attr('src', chocolate.image).attr('width', '50').attr('height', '50')));
                row.append($('<td>').text(chocolate.name));
                row.append($('<td>').text(chocolate.description));
                row.append($('<td>').text(chocolate.weight + 'g'));
                row.append($('<td>').text(chocolate.type.toLowerCase()));
                row.append($('<td>').text(chocolate.category.toLowerCase()));
                row.append($('<td>').text(chocolate.count));
                var deleteButton = $('<button>').text('Delete chocolate');
                deleteButton.click(function () {
                    deleteChocolate(chocolate.name);
                });
                row.append($('<td>').append(deleteButton));

                var updateButton = $('<button>').text('Update chocolate');
                updateButton.click(function () {
                    createUpdateForm(chocolate.name);
                });
                row.append($('<td>').append(updateButton));

                tableBody.append(row);
            });
        }
    });

});

function createUpdateForm(chocolateName) {
    const selectedChocolate = chocolates.find(chocolate => chocolate.name === chocolateName);

    $('#updateName').val(selectedChocolate.name);
    $('#updateWeight').val(selectedChocolate.weight);
    $('#updateChocolateType').val(selectedChocolate.type);
    $('#updateChocolateCategory').val(selectedChocolate.category);
    $('#updateDescription').val(selectedChocolate.description);

    $('#chocolateUpdateForm').show();
}

function updateChocolate() {
    return function (event) {
        event.preventDefault();

        let formData = new FormData();
        formData.append('name', $('#updateName').val());
        formData.append('description', $('#updateDescription').val());
        formData.append('weight', $('#updateWeight').val());
        formData.append('type', $('#updateChocolateType').val());
        formData.append('category', $('#updateChocolateCategory').val());
        formData.append('image', $('#updateImage')[0].files[0]);

        $.ajax({
            url: '/wp-project-2024/rest/chocolates/',
            type: 'PUT',
            data: formData,
            processData: false,
            contentType: false,
            success: function () {
                window.location.href = "http://localhost:8080/wp-project-2024/chocolates.html";
            }
        });
    }
}

function deleteChocolate(name) {
    $.ajax({
        url: `/wp-project-2024/rest/chocolates/` + name,
        type: 'DELETE',
        success: function (response) {
            console.log('Deletion successful', response);
            window.location.href = "http://localhost:8080/wp-project-2024/chocolates.html";
        },
        error: function (xhr, status, error) {
            console.error('Error occurred during deletion', error);
        }
    });
}

function fetchData() {
    return $.get('/wp-project-2024/rest/status');
}

function fetchManagerData(id) {
    return $.get('/wp-project-2024/rest/managers/' + id);
}


function register() {
    return function (event) {
        event.preventDefault();

        let formData = new FormData();
        formData.append('name', $('#inputName').val());
        formData.append('description', $('#inputDescription').val());
        formData.append('weight', $('#inputWeight').val());
        formData.append('type', $('#inputChocolateType').val());
        formData.append('category', $('#inputChocolateCategory').val());
        formData.append('image', $('#inputImage')[0].files[0]);

        let factoryName = '';

        fetchData()
            .then(function (data) {
                return fetchManagerData(data.id);
            })
            .then(function (managerData) {
                factoryName = managerData.factory.name;
                console.log('Manager data:', managerData);
        
                formData.append('factoryName', factoryName);
        
                $.ajax({
                    url: '/wp-project-2024/rest/chocolates/',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.href = "http://localhost:8080/wp-project-2024/chocolates.html";
                    }
                });
            })
            .catch(function (error) {
                console.error('Error fetching data:', error);
            });
        
    }
}


