$(document).ready(function () {
    $('#form-register').submit(
        register());
});

function register() {
    return function (event) {
        event.preventDefault();


        let username = $('#inputUsername').val();
        let password = $('#inputPassword').val();
        let password2 = $('#inputPassword2').val();
        let firstName = $('#inputFirstName').val();
        let lastName = $('#inputLastName').val();
        let gender = $('#gender').val();
        let dateOfBirth = $('#dateOfBirth').val();

        if (password != password2) {
            alert("Error! Password did not match.");
            return;
        }

        $.post({
            url: '/wp-project-2024/rest/register', data: JSON.stringify({
                username: username, password: password, firstName: firstName, lastName: lastName, gender: gender,
                dateOfBirth: dateOfBirth, role: 'BUYER'
            }),
            contentType: 'application/json', success: function () {
                window.location.href = "http://localhost:8080/wp-project-2024/homepage.html";
            }
        });
    }
}