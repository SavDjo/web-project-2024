let chocolates = [];
var selectedChocolate;

$(document).ready(function () {
    $('#countForm').submit(
        updateChocolate());

    $.get({
        url: '/wp-project-2024/rest/status', success: function (data) {
            const workerId = data.id;
            $.get({
                url: '/wp-project-2024/rest/workers/' + workerId, success: function (data) {
                    chocolates.push(data.factory.chocolates);
                    var tableBody = $('#tableBody');
                    data.factory.chocolates.forEach(function (chocolate) {
                        chocolates.push(chocolate);
                        var row = $('<tr>');
                        row.append($('<td>').append($('<img>').attr('src', chocolate.image).attr('width', '50').attr('height', '50')));
                        row.append($('<td>').text(chocolate.name));
                        row.append($('<td>').text(chocolate.description));
                        row.append($('<td>').text(chocolate.weight + 'g'));
                        row.append($('<td>').text(chocolate.type.toLowerCase()));
                        row.append($('<td>').text(chocolate.category.toLowerCase()));
                        row.append($('<td>').text(chocolate.count));
                        var changeCountButton = $('<button>').text('Change chocolate count');
                        changeCountButton.click(function () {
                            changeChocolateCount(chocolate.name);
                        });
                        row.append($('<td>').append(changeCountButton));
        
                        tableBody.append(row);
                    });
                }
            });
        }
    });
});

function changeChocolateCount(name) {
    selectedChocolate = chocolates.find(chocolate => chocolate.name === name);

    $('#inputCount').val(selectedChocolate.count);

    $('#countForm').show();
}

function updateChocolate() {
    return function (event) {
        event.preventDefault();

        let count = $('#inputCount').val();

        $.ajax({
            url: '/wp-project-2024/rest/chocolates/' + selectedChocolate.name,
            type: 'PUT',
            data: JSON.stringify({
                count: count
            }),
            contentType: 'application/json',
            success: function () {
                window.location.href = "http://localhost:8080/wp-project-2024/chocolate-stock.html";
            }
        });
    }
}



