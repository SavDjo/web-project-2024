package model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class Order {
	
	private int id;
	private List<Chocolate> chocolates;
	private ChocolateFactory factory;
	private LocalDate purchaseTime;
	private double price;
	private Buyer buyer;
	private OrderStatus status;
	private boolean isDeleted;
	private String rejectionReason;
	
	public Order() {
	}

	public Order(int id, List<Chocolate> chocolates, ChocolateFactory factory, LocalDate purchaseTime, double price,
			Buyer buyer, OrderStatus status) {
		super();
		this.id = id;
		this.chocolates = chocolates;
		this.factory = factory;
		this.purchaseTime = purchaseTime;
		this.price = price;
		this.buyer = buyer;
		this.status = status;
		this.isDeleted = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Chocolate> getChocolates() {
		return chocolates;
	}

	public void setChocolates(List<Chocolate> chocolates) {
		this.chocolates = chocolates;
	}

	public ChocolateFactory getFactory() {
		return factory;
	}

	public void setFactory(ChocolateFactory factory) {
		this.factory = factory;
	}

	public LocalDate getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(LocalDate purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
	

}
