package model;

import java.time.LocalDate;
import java.util.List;

public class Buyer extends User {
	
	private List<Order> orders;
	private ShoppingCart cart;
	private BuyerType type;
	private int points;
	
	public Buyer() {
		super();
		this.role = Role.BUYER;
	}

	public Buyer(String username, String password, String firstName, String lastName, Gender gender, LocalDate dateOfBirth, List<Order> orders, ShoppingCart cart, BuyerType type, int points) {
		super(username, password, firstName, lastName, gender, dateOfBirth, Role.BUYER);
		this.orders = orders;
		this.cart = cart;
		this.type = type;
		this.points = points;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public ShoppingCart getCart() {
		return cart;
	}

	public BuyerType getType() {
		return type;
	}

	public void setType(BuyerType type) {
		this.type = type;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}

}
