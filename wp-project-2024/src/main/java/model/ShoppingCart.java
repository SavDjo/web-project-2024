package model;

public class ShoppingCart {
	private Chocolate chocolate;
	private Buyer buyer;
	private int price;
	
	public ShoppingCart() {}

	public ShoppingCart(Chocolate chocolate, Buyer buyer, int price) {
		super();
		this.chocolate= chocolate;
		this.buyer = buyer;
		this.price = price;
	}

	public Chocolate getChocolate() {
		return chocolate;
	}

	public void setVehicle(Chocolate chocolate) {
		this.chocolate = chocolate;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
