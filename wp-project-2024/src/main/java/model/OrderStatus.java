package model;

public enum OrderStatus {
    BEING_PROCESSED("Being processed"),
    APPROVED("Approved"),
    REFUSED("Refused"),
    CANCELLED("Cancelled");
	
    public final String label;

    private OrderStatus(String label) {
        this.label = label;
    }
}
