package model;

import java.time.LocalDate;

public class Worker extends User {
	private ChocolateFactory factory;
	
	public Worker() {
		super();
		this.role=Role.WORKER;
	}
	
	public Worker(String username, String password, String firstName, String lastName, Gender gender, LocalDate dateOfBirth) {
		super(username, password, firstName, lastName, gender, dateOfBirth, Role.WORKER);
	}

	public ChocolateFactory getFactory() {
		return factory;
	}

	public void setFactory(ChocolateFactory factory) {
		this.factory = factory;
	}
}
