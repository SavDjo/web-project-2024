package model;

import java.time.LocalDate;

public class Administrator extends User {
	
	public Administrator() {
		super();
		role=Role.ADMINISTRATOR;
	}
	
	public Administrator(String username, String password, String firstName, String lastName, Gender gender, LocalDate dateOfBirth) {
		super(username, password, firstName, lastName, gender, dateOfBirth, Role.ADMINISTRATOR);
	}

}
