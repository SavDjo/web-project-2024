package model;

public enum ChocolateType {
    WHITE("White"),
    DARK("Dark"),
    MILK("Milk");
	
    public final String label;

    private ChocolateType(String label) {
        this.label = label;
    }
}
