package model;

import java.time.LocalDate;

public class Manager extends User {
	
	private ChocolateFactory factory;
	
	public Manager() {
		super();
		this.role=Role.MANAGER;
	}
	
	public Manager(String username, String password, String firstName, String lastName, Gender gender, LocalDate dateOfBirth) {
		super(username, password, firstName, lastName, gender, dateOfBirth, Role.MANAGER);
	}

	public ChocolateFactory getFactory() {
		return factory;
	}

	public void setFactory(ChocolateFactory factory) {
		this.factory = factory;
	}
	
	
}
