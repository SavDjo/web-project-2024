package model;

public class Chocolate {
	private int id;
	private double price;
	private String name;
	private ChocolateType type;
	private double weight;
	private String description;
	private String image;
	private ChocolateStatus status;
	private int count;
	private ChocolateCategory category;
	private boolean isDeleted;
	
	public Chocolate() {
	}

	public Chocolate(int id, double price, String name, ChocolateType type, double weight, String description, String image,
			ChocolateStatus status, int count, ChocolateCategory category) {
		super();
		this.id = id;
		this.price = price;
		this.name = name;
		this.type = type;
		this.weight = weight;
		this.description = description;
		this.image = image;
		this.status = status;
		this.count = count;
		this.category = category;
		this.isDeleted = false;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ChocolateType getType() {
		return type;
	}

	public void setType(ChocolateType type) {
		this.type = type;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ChocolateStatus getStatus() {
		return status;
	}

	public void setStatus(ChocolateStatus status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public ChocolateCategory getCategory() {
		return category;
	}

	public void setCategory(ChocolateCategory category) {
		this.category = category;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
