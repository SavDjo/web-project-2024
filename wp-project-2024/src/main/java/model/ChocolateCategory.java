package model;

public enum ChocolateCategory {
    REGULAR("Regular"),
    COOKING("Cooking"),
    DRINKING("Drinking");
	
    public final String label;

    private ChocolateCategory(String label) {
        this.label = label;
    }
}
