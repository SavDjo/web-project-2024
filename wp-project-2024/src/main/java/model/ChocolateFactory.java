package model;

import java.time.LocalTime;
import java.util.List;

public class ChocolateFactory {
	private int id;
	private String name;
	private List<Chocolate> chocolates;
	private Location location;
	private String logo;
    private LocalTime openingTime;
    private LocalTime closingTime;
	private double rating;
	private ChocolateFactoryStatus status;
	private boolean isDeleted;
	
	public ChocolateFactory() {
	}

	public ChocolateFactory(int id, String name, List<Chocolate> chocolates, Location location, String logo, LocalTime openingTime,
			LocalTime closingTime, double rating, ChocolateFactoryStatus status) {
		super();
		this.id = id;
		this.name = name;
		this.chocolates = chocolates;
		this.location = location;
		this.logo = logo;
		this.openingTime = openingTime;
		this.closingTime = closingTime;
		this.rating = rating;
		this.status = status;
		this.isDeleted = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setStatus(ChocolateFactoryStatus status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Chocolate> getChocolates() {
		return chocolates;
	}

	public void setChocolates(List<Chocolate> chocolates) {
		this.chocolates = chocolates;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public LocalTime getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(LocalTime openingTime) {
		this.openingTime = openingTime;
	}

	public LocalTime getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(LocalTime closingTime) {
		this.closingTime = closingTime;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public ChocolateFactoryStatus getStatus() {
        LocalTime currentTime = LocalTime.now();

        if (currentTime.isAfter(openingTime) && currentTime.isBefore(closingTime)) {
            return ChocolateFactoryStatus.OPEN;
        } else {
            return ChocolateFactoryStatus.CLOSED;
        }
	}

	public void setStatus() {
        LocalTime currentTime = LocalTime.now();

        if (currentTime.isAfter(openingTime) && currentTime.isBefore(closingTime)) {
            this.status = ChocolateFactoryStatus.OPEN;
        } else {
            this.status = ChocolateFactoryStatus.CLOSED;
        }
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
