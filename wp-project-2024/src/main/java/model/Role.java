package model;

public enum Role {
    ADMINISTRATOR("Administrator"),
    MANAGER("Manager"),
    BUYER("Buyer"),
    WORKER("Worker");
	
    public final String label;

    private Role(String label) {
        this.label = label;
    }
}
