package model;

public enum ChocolateFactoryStatus {
    OPEN("Open"),
    CLOSED("Closed");
	
    public final String label;

    private ChocolateFactoryStatus(String label) {
        this.label = label;
    }
}
