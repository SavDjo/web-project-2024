package model;

public enum ChocolateStatus {
    IN_STOCK("In stock"),
    OUT_OF_STOCK("Out of stock");
	
    public final String label;

    private ChocolateStatus(String label) {
        this.label = label;
    }
}
