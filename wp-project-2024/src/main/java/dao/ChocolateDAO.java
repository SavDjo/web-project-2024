package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.Chocolate;
import model.ChocolateFactory;

public class ChocolateDAO {
	private Map<Integer, Chocolate> chocolates = new HashMap<>();
	private String contextPath;
	private ChocolateFactoryDAO chocolateFactoryDAO;
	private ManagerDAO managerDAO;
	private WorkerDAO workerDAO;
	
	public ChocolateDAO() {}

	public ChocolateDAO(String contextPath) {
		this.contextPath = contextPath;
		workerDAO = new WorkerDAO(contextPath);
		managerDAO = new ManagerDAO(contextPath);
		chocolateFactoryDAO = new ChocolateFactoryDAO(contextPath);
		loadChocolates();
	}
	
	public Chocolate getChocolateById(int id) {
		if (!chocolates.containsKey(id)) {
			return null;
		}
		Chocolate chocolate = chocolates.get(id);
		return chocolate;
	}
	
	public Chocolate getChocolateByName(String name) {
		List<Chocolate> chocolateList = new ArrayList<>(chocolates.values());
		for(Chocolate chocolate : chocolateList) {
			if(chocolate.getName().equals(name)) {
				return chocolate;
			}
		}
		return null;
	}
	
	public List<Chocolate> getAllChocolates() {
		List<Chocolate> chocolateList = new ArrayList<>(chocolates.values());
		return chocolateList;
	}
	
	public void addChocolate(Chocolate chocolate, String factoryName) {
		if (!chocolateExists(chocolate)) {
			int id = chocolates.values().size();
			chocolate.setId(id);
			chocolates.put(id, chocolate);
			saveChocolates();
	        chocolateFactoryDAO.addChocolate(chocolate, factoryName);
	        managerDAO.addChocolate(chocolate, factoryName);
	        workerDAO.addChocolate(chocolate, factoryName);
		}
	}
	
	public boolean updateChocolate(Chocolate chocolate) {
	    if (chocolates.containsKey(chocolate.getId())) {
	    	Chocolate chocolateForUpdating = getChocolateById(chocolate.getId());
		    chocolateForUpdating.setName(chocolate.getName());
		    chocolateForUpdating.setWeight(chocolate.getWeight());
		    chocolateForUpdating.setDescription(chocolate.getDescription());
		    chocolateForUpdating.setCount(chocolate.getCount());
		    chocolateForUpdating.setType(chocolate.getType());
		    chocolateForUpdating.setCategory(chocolate.getCategory());
		    if(chocolate.getImage() != null) {
			    chocolateForUpdating.setImage(chocolate.getImage());
		    }
		    
	        chocolates.put(chocolate.getId(), chocolateForUpdating);
	        saveChocolates();
	        chocolateFactoryDAO.updateChocolates(chocolate);
	        managerDAO.updateChocolates(chocolate);
	        workerDAO.updateChocolates(chocolate);
	        return true;
	    } else {
	        return false;
	    }
	}
	
	public boolean chocolateExists(Chocolate chocolate) {
		return chocolates.containsValue(chocolate);
	}
	
	public void deleteChocolate(int id) {
		Chocolate chocolate = chocolates.get(id);
		chocolate.setDeleted(true);
		updateChocolate(chocolate);
	}
	
	public void saveChocolates() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/chocolates.txt");
			file.createNewFile();
			
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			
			ObjectMapper mapper = new ObjectMapper();
		    mapper.setDateFormat(df);
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(chocolates);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadChocolates() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/chocolates.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Chocolate.class);
			chocolates = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/chocolates.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(chocolates);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
