package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.Administrator;
import model.Buyer;

public class AdministratorDAO {
	
	private Map<Integer, Administrator> administrators = new HashMap<>();
	private String contextPath;
	
	public AdministratorDAO() {}

	public AdministratorDAO(String contextPath) {
		this.contextPath = contextPath;
		loadAdministrators();
	}
	
	public Administrator getAdministratorByUsernameAndPassword(String username, String password) {
		List<Administrator> administratorList = new ArrayList<>(administrators.values());
		for(Administrator administrator : administratorList) {
			if(administrator.getUsername().equals(username) && administrator.getPassword().equals(password)) {
				return administrator;
			}
		}
		return null;
	}
	
	public Administrator getAdministratorByUsername(String username) {
		List<Administrator> administratorList = new ArrayList<>(administrators.values());
		for(Administrator administrator : administratorList) {
			if(administrator.getUsername().equals(username)) {
				return administrator;
			}
		}
		return null;
	}
	
	public Collection<Administrator> getAllAdministrators() {
		return administrators.values();
	}
	
	public boolean updateAdministrator(Administrator admin) {
	    if (administrators.containsKey(admin.getId())) {
		    
	        administrators.put(admin.getId(), admin);
	        saveAdministrators();
	        return true;
	    } else {
	        return false;
	    }
	}
	
	public boolean administratorExists(Administrator administrator) {
		return administrators.containsValue(administrator);
	}
	
	public void saveAdministrators() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/administrators.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(administrators);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadAdministrators() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/administrators.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Administrator.class);
			administrators = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/administrators.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(administrators);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
