package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.ChocolateFactory;
import model.Order;

public class OrderDAO {
	private Map<Integer, Order> orders = new HashMap<>();
	private String contextPath;
	private BuyerDAO buyerDAO;
	
	public OrderDAO() {}

	public OrderDAO(String contextPath) {
		this.contextPath = contextPath;
		buyerDAO = new BuyerDAO(contextPath);
		loadOrders();
	}
	
	public Order getOrderById(int id) {
		if (!orders.containsKey(id)) {
			return null;
		}
		Order order = orders.get(id);
		return order;
	}
	
	public List<Order> getAllOrders() {
		List<Order> orderList = new ArrayList<>(orders.values());
		return orderList;
	}
	
	public List<Order> getOrdersFromFactory(String factoryName) {
		List<Order> orderList = new ArrayList<>();
		for(Order order : orders.values()) {
			if(order.getFactory().getName().equals(factoryName)) {
				orderList.add(order);
			}
		}
		return orderList;
	}
	
	public List<Order> getOrdersFromUser(String username) {
		List<Order> orderList = new ArrayList<>();
		for(Order order : orders.values()) {
			if(order.getBuyer().getUsername().equals(username)) {
				orderList.add(order);
			}
		}
		return orderList;
	}
	
	public void addOrder(Order order, String userName) {
		if (!orderExists(order)) {
			int id = orders.values().size();
			order.setId(id);
			orders.put(id, order);
			saveOrders();
	        buyerDAO.addOrder(order, userName);
		}
	}
	

	public boolean updateOrders(Order order) {
	    if (orders.containsKey(order.getId())) {
	    	Order orderForUpdating = getOrderById(order.getId());
		    orderForUpdating.setBuyer(order.getBuyer());
		    orderForUpdating.setChocolates(order.getChocolates());
		    orderForUpdating.setFactory(order.getFactory());
		    orderForUpdating.setPrice(order.getPrice());
		    orderForUpdating.setPurchaseTime(order.getPurchaseTime());
		    orderForUpdating.setStatus(order.getStatus());
		    
	        orders.put(order.getId(), orderForUpdating);
	        saveOrders();
	        buyerDAO.updateOrders(order);
	        return true;
	    } else {
	        return false;
	    }
	}
	
	public boolean orderExists(Order order) {
		return orders.containsValue(order);
	}
	
	public void deleteOrder(String name) {
		Order order = orders.get(name);
		order.setDeleted(true);
		updateOrders(order);
	}
	
	public void saveOrders() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/orders.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(orders);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadOrders() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/orders.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Order.class);
			orders = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/orders.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(orders);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
