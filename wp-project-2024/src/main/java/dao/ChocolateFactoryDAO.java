package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.Chocolate;
import model.ChocolateCategory;
import model.ChocolateFactory;
import model.ChocolateFactoryStatus;
import model.ChocolateStatus;
import model.ChocolateType;
import model.Location;
import model.Manager;
import model.Order;
import model.Worker;

public class ChocolateFactoryDAO {
	private Map<Integer, ChocolateFactory> chocolateFactories = new HashMap<>();
	private String contextPath;
	
	public ChocolateFactoryDAO() {}

	public ChocolateFactoryDAO(String contextPath) {
		this.contextPath = contextPath;
		loadFactories();
	}
	
	public ChocolateFactory getFactoryById(int id) {
		if (!chocolateFactories.containsKey(id)) {
			return null;
		}
		ChocolateFactory chocolateFactory = chocolateFactories.get(id);
		return chocolateFactory;
	}
	
	public ChocolateFactory getFactoryByName(String name) {
		List<ChocolateFactory> factoryList = new ArrayList<>(chocolateFactories.values());
		for(ChocolateFactory factory : factoryList) {
			if(factory.getName().equals(name)) {
				return factory;
			}
		}
		return null;
	}
	
	public boolean updateFactory(ChocolateFactory factory) {
	    if (chocolateFactories.containsKey(factory.getId())) {
	    	ChocolateFactory factoryForUpdating = getFactoryById(factory.getId());
		    factoryForUpdating.setRating(factory.getRating());
		    
	        chocolateFactories.put(factory.getId(), factoryForUpdating);
	        saveFactories();
	        return true;
	    } else {
	        return false;
	    }
	}

	
	public List<ChocolateFactory> getAllFactories() {
		loadFactories();
		List<ChocolateFactory> sortedFactories = new ArrayList<>(chocolateFactories.values());
		for(int i = 0; i < sortedFactories.size(); i++) {
			if(sortedFactories.get(i).getStatus() == ChocolateFactoryStatus.CLOSED) {
				sortedFactories.get(i).setStatus();
				ChocolateFactory temp = sortedFactories.get(i);
				sortedFactories.remove(i);
				sortedFactories.add(temp);
			} else {
				sortedFactories.get(i).setStatus();
			}
		}
		return sortedFactories;
	}
	
	public List<ChocolateFactory> getFactoriesFromOrders(List<Order> orders) {
		loadFactories();
		List<ChocolateFactory> factoryList = new ArrayList<>(chocolateFactories.values());
		List<ChocolateFactory> rateableFactories = new ArrayList<>();
		for(ChocolateFactory factory : factoryList) {
			for(Order order : orders) {
				if(order.getFactory().getName().equals(factory.getName())) {
					rateableFactories.add(factory);
				}
			}
		}
		return rateableFactories;
	}
	
	public void updateChocolates(Chocolate updatedChocolate) {
	    for (ChocolateFactory factory : chocolateFactories.values()) {
	        List<Chocolate> chocolates = factory.getChocolates();
	        for (int i = 0; i < chocolates.size(); i++) {
	            Chocolate existingChocolate = chocolates.get(i);
	            if (existingChocolate.getId() == updatedChocolate.getId()) {
	                chocolates.set(i, updatedChocolate);
	                factory.setChocolates(chocolates);
	                chocolateFactories.replace(factory.getId(), factory);
	                
	                break;
	            }
	        }
	    }
	    saveFactories();
	}
	
	public void addChocolate(Chocolate chocolate, String factoryName) {
		for (ChocolateFactory factory : chocolateFactories.values()) {
			if (factory.getName().equals(factoryName)) {
				List<Chocolate> chocolates = factory.getChocolates();
				chocolates.add(chocolate);
				factory.setChocolates(chocolates);
            	chocolateFactories.replace(factory.getId(), factory);
			}
		}
	    saveFactories();
	}
	
	public void addChocolateFactory(ChocolateFactory chocolateFactory) {
		int id = chocolateFactories.values().size();
		chocolateFactory.setId(id);
		chocolateFactories.put(id, chocolateFactory);
		saveFactories();
	}
	
	public void saveFactories() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/chocolateFactories.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(chocolateFactories);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadFactories() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/chocolateFactories.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, ChocolateFactory.class);
			chocolateFactories = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/chocolateFactories.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(chocolateFactories);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
