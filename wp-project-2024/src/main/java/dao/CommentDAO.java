package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.Chocolate;
import model.Comment;

public class CommentDAO {
	
	private Map<Integer, Comment> comments = new HashMap<>();
	private String contextPath;
	
	public CommentDAO() {}

	public CommentDAO(String contextPath) {
		this.contextPath = contextPath;
		loadComments();
	}
	
	public Comment getCommentById(int id) {
	    return comments.get(id);
	}
	
	public List<Comment> getAllComments() {
		List<Comment> commentList = new ArrayList<>(comments.values());
		return commentList;
	}
	
	public List<Comment> getCommentsForFactory(String factoryName) {
		List<Comment> commentList = new ArrayList<>();
		for(Comment comment : comments.values()) {
			if(comment.getFactory().equals(factoryName) && comment.isApproved()) {
				commentList.add(comment);
			}
		}
		return commentList;
	}
	
	public double getAverageRatingForFactory(String factoryName) {
		int count = 0;
		double ratingSum = 0;
		for(Comment comment : comments.values()) {
			if(comment.getFactory().equals(factoryName)) {
				count++;
				ratingSum += comment.getRating();
			}
		}
		if(count != 0) {
			return ratingSum/count;
		}
		return 0;
	}
	
	public boolean alreadyRated(String factoryName, String username) {
		for(Comment comment : comments.values()) {
			if(comment.getFactory().equals(factoryName) && comment.getAuthor().equals(username)) {
				return true;
			}
		}
		return false;
	}
	
	public List<Comment> getApprovedComments() {
		List<Comment> commentList = new ArrayList<>();
		for(Comment comment : comments.values()) {
			if(comment.isApproved() == true) {
				commentList.add(comment);
			}
		}
		return commentList;
	}
	
	public List<Comment> getNotApprovedComments() {
		List<Comment> commentList = new ArrayList<>();
		for(Comment comment : comments.values()) {
			if(comment.isApproved() == false) {
				commentList.add(comment);
			}
		}
		return commentList;
	}
	
	public void addComment(Comment comment) {
		int id = comments.values().size();
		comment.setId(id);
		comments.put(id, comment);
		saveComments();
	}
	
	public void updateComment(Comment comment) {
	    if (comments.containsKey(comment.getId())) {
	        comments.put(comment.getId(), comment);
	        saveComments();
	    } else {
	        System.err.println("Comment with ID " + comment.getId() + " doesn't exist.");
	    }
	}
	
	public void deleteComment(int id) {
		Comment comment = comments.get(id);
		comment.setDeleted(true);
		updateComment(comment);
	}
	
	public boolean commentExists(Comment comment) {
		return comments.containsValue(comment);
	}
	
	public void saveComments() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/comments.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(comments);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadComments() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/comments.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Comment.class);
			comments = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/comments.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(comments);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
