package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.Administrator;
import model.Chocolate;
import model.ChocolateFactory;
import model.Manager;

public class ManagerDAO {
	
	private Map<Integer, Manager> managers = new HashMap<>();
	private String contextPath;
	
	public ManagerDAO() {}

	public ManagerDAO(String contextPath) {
		this.contextPath = contextPath;
		loadManagers();
	}
	
	public Manager getManagerByUsernameAndPassword(String username, String password) {
		List<Manager> managerList = new ArrayList<>(managers.values());
		for(Manager manager : managerList) {
			if(manager.getUsername().equals(username) && manager.getPassword().equals(password)) {
				return manager;
			}
		}
		return null;
	}
	
	public Manager getManagerByUsername(String username) {
		List<Manager> managerList = new ArrayList<>(managers.values());
		for(Manager manager : managerList) {
			if(manager.getUsername().equals(username)) {
				return manager;
			}
		}
		return null;
	}
	
	public Manager getManagerById(int id) {
	    return managers.get(id);
	}
	
	public List<Manager> getAllManagers() {
		List<Manager> managerList = new ArrayList<>(managers.values());
		return managerList;
	}
	
	public void addManager(Manager manager) {
		int id = managers.values().size();
		manager.setId(id);
		managers.put(id, manager);
		saveManagers();
	}
	
	public void updateManager(Manager manager) {
	    if (managers.containsKey(manager.getId())) {
	        managers.put(manager.getId(), manager);
	        saveManagers();
	    } else {
	        System.err.println("Manager with ID " + manager.getId() + " doesn't exist.");
	    }
	}
	
	public void updateChocolates(Chocolate updatedChocolate) {
		for (Manager manager : managers.values()) {
			List<Chocolate> chocolates = manager.getFactory().getChocolates();
				for (int i = 0; i < chocolates.size(); i++) {
					Chocolate existingChocolate = chocolates.get(i);
					if (existingChocolate.getId() == updatedChocolate.getId()) {
						chocolates.set(i, updatedChocolate);
	                	manager.getFactory().setChocolates(chocolates);
	                	managers.replace(manager.getId(), manager);
	                	break;
					}
				}
		}
	    saveManagers();
	}
	
	public void addChocolate(Chocolate chocolate, String factoryName) {
		for (Manager manager : managers.values()) {
			if(manager.getFactory().getName() == factoryName) {
				ChocolateFactory factory = manager.getFactory();
				List<Chocolate> chocolates = manager.getFactory().getChocolates();
				chocolates.add(chocolate);
				factory.setChocolates(chocolates);
				manager.setFactory(factory);
            	managers.replace(manager.getId(), manager);
			}
		}
	    saveManagers();
	}
	
	public boolean managerExists(Manager manager) {
		return managers.containsValue(manager);
	}
	
	public void saveManagers() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/managers.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(managers);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadManagers() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/managers.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Manager.class);
			managers = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/managers.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(managers);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
