package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import model.Chocolate;
import model.ChocolateFactory;
import model.Manager;
import model.Worker;

public class WorkerDAO {
	
	private Map<Integer, Worker> workers = new HashMap<>();
	private String contextPath;
	
	public WorkerDAO() {}

	public WorkerDAO(String contextPath) {
		this.contextPath = contextPath;
		loadWorkers();
	}
	
	public Worker getWorkerByUsernameAndPassword(String username, String password) {
		loadWorkers();
		List<Worker> workerList = new ArrayList<>(workers.values());
		for(Worker worker : workerList) {
			if(worker.getUsername().equals(username) && worker.getPassword().equals(password)) {
				return worker;
			}
		}
		return null;
	}
	
	public Worker getWorkerByUsername(String username) {
		List<Worker> buyerList = new ArrayList<>(workers.values());
		for(Worker buyer : buyerList) {
			if(buyer.getUsername().equals(username)) {
				return buyer;
			}
		}
		return null;
	}
	
	public Worker getWorkerById(int id) {
	    return workers.get(id);
	}
	
	public List<Worker> getAllWorkers() {
		List<Worker> workerList = new ArrayList<>(workers.values());
		return workerList;
	}
	
	public List<Worker> getFactories() {
		List<Worker> workerList = new ArrayList<>(workers.values());
		return workerList;
	}
	
	public void addWorker(Worker worker) {
		int id = workers.values().size();
		worker.setId(id);
		workers.put(id, worker);
		saveWorkers();
	}
	
	public void deleteWorker(int id) {
		Worker worker = workers.get(id);
		worker.setDeleted(true);
		updateWorker(worker);
	}
	
	public void updateWorker(Worker worker) {
	    if (workers.containsKey(worker.getId())) {
	        workers.put(worker.getId(), worker);
	        saveWorkers();
	    } else {
	        System.err.println("Worker with ID " + worker.getId() + " doesn't exist.");
	    }
	}
	
	public void updateChocolates(Chocolate updatedChocolate) {
		for (Worker worker : workers.values()) {
			List<Chocolate> chocolates = worker.getFactory().getChocolates();
				for (int i = 0; i < chocolates.size(); i++) {
					Chocolate existingChocolate = chocolates.get(i);
					if (existingChocolate.getId() == updatedChocolate.getId()) {
						chocolates.set(i, updatedChocolate);
	                	worker.getFactory().setChocolates(chocolates);
	                	workers.replace(worker.getId(), worker);
	                	break;
					}
				}
		}
	    saveWorkers();
	}
	
	public void addChocolate(Chocolate chocolate, String factoryName) {
		for (Worker worker : workers.values()) {
			if(worker.getFactory().getName().equals(factoryName)) {
				ChocolateFactory factory = worker.getFactory();
				List<Chocolate> chocolates = worker.getFactory().getChocolates();
				chocolates.add(chocolate);
				factory.setChocolates(chocolates);
				worker.setFactory(factory);
            	workers.replace(worker.getId(), worker);
			}
		}
	    saveWorkers();
	}
	
	public boolean workerExists(Worker worker) {
		return workers.containsValue(worker);
	}
	
	public void saveWorkers() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/workers.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(workers);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadWorkers() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/workers.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Worker.class);
			workers = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/workers.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(workers);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}
