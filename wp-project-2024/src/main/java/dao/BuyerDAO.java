package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import model.Buyer;
import model.Chocolate;
import model.ChocolateFactory;
import model.Order;

public class BuyerDAO {
	
	private Map<Integer, Buyer> buyers = new HashMap<>();
	private String contextPath;
	
	public BuyerDAO() {}

	public BuyerDAO(String contextPath) {
		this.contextPath = contextPath;
		loadBuyers();
	}
	
	public Buyer getBuyerByUsernameAndPassword(String username, String password) {
		List<Buyer> buyerList = new ArrayList<>(buyers.values());
		for(Buyer buyer : buyerList) {
			if(buyer.getUsername().equals(username) && buyer.getPassword().equals(password)) {
				return buyer;
			}
		}
		return null;
	}
	
	public Buyer getBuyerByUsername(String username) {
		List<Buyer> buyerList = new ArrayList<>(buyers.values());
		for(Buyer buyer : buyerList) {
			if(buyer.getUsername().equals(username)) {
				return buyer;
			}
		}
		return null;
	}
	
	public void addOrder(Order order, String username) {
		for (Buyer buyer : buyers.values()) {
			if (buyer.getUsername().equals(username)) {
				List<Order> orders = buyer.getOrders();
				orders.add(order);
				buyer.setOrders(orders);
            	buyers.replace(buyer.getId(), buyer);
			}
		}
	    saveBuyers();
	}
	
	public Collection<Buyer> getAllBuyers() {
		return buyers.values();
	}
	
	public void addBuyer(Buyer buyer) {
		int id = buyers.values().size();
		buyer.setId(id);
		buyers.put(id, buyer);
		saveBuyers();
	}
	
	public boolean buyerExists(Buyer buyer) {
		return buyers.containsValue(buyer);
	}
	
	public boolean updateBuyer(Buyer buyer) {
	    if (buyers.containsKey(buyer.getId())) {
		    
	        buyers.put(buyer.getId(), buyer);
	        saveBuyers();
	        return true;
	    } else {
	        return false;
	    }
	}
	
	public void updateOrders(Order updatedOrder) {
	    for (Buyer buyer : buyers.values()) {
	        List<Order> orders = buyer.getOrders();
	        for (int i = 0; i < orders.size(); i++) {
	            Order existingOrder = orders.get(i);
	            if (existingOrder.getId() == updatedOrder.getId()) {
	                orders.set(i, updatedOrder);
	                buyer.setOrders(orders);
	                buyers.replace(buyer.getId(), buyer);
	                
	                break;
	            }
	        }
	    }
	    saveBuyers();
	}
	
	public void saveBuyers() {
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/buyers.txt");
			file.createNewFile();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			
			String json = mapper.writeValueAsString(buyers);
			
			fw = new FileWriter(file);
			fw.write(json);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				}
				catch (Exception e) { }
				}
			}
		}
	
	public void loadBuyers() {
		BufferedReader in = null;
		FileWriter fw = null;
		try {
			File file = new File(contextPath + "/buyers.txt");
			in = new BufferedReader(new FileReader(file));
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			TypeFactory typeFactory = mapper.getTypeFactory();
			MapType mapType = typeFactory.constructMapType(HashMap.class, Integer.class, Buyer.class);
			buyers = mapper.readValue(file, mapType);
		} catch (FileNotFoundException fex) {
			try {
				File file = new File(contextPath + "/buyers.txt");
				file.createNewFile();
				
				String json = new ObjectMapper().writeValueAsString(buyers);
				
				fw = new FileWriter(file);
				fw.write(json);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (fw != null) {
					try {
						fw.close();
					}
					catch (Exception e) { }
					}
				}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Exception e) { }
				}
			}
		}
}

