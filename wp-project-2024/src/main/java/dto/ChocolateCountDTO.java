package dto;

public class ChocolateCountDTO {
    private int count;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
