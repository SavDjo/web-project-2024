package services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.WorkerDAO;
import model.Worker;
import model.ChocolateFactory;


@Path("workers")
public class WorkerService {
	@Context
	ServletContext ctx;
	
	public WorkerService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("workerDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("workerDAO", new WorkerDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Worker> getWorkers() {
		WorkerDAO dao = (WorkerDAO) ctx.getAttribute("workerDAO");
		List<Worker> existingWorkers = new ArrayList<>();
		for(Worker worker : dao.getAllWorkers()) {
			if(worker.isDeleted() == false) {
				existingWorkers.add(worker);
			}
		}
		return existingWorkers;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Worker getWorker(@PathParam("id") int id) {
		WorkerDAO dao = (WorkerDAO) ctx.getAttribute("workerDAO");
		return dao.getWorkerById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addWorker(Worker worker, @Context HttpServletRequest request) {
		WorkerDAO dao = (WorkerDAO) ctx.getAttribute("workerDAO");
		if(dao.workerExists(dao.getWorkerByUsername(worker.getUsername()))) {
			return Response.status(400).entity("Worker is already registered.").build();
		} else {
			dao.addWorker(worker);
			return Response.status(200).build();
		}
	}
	
	@PUT
	@Path("/factory/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setWorkerFactory(@PathParam("username") String username, ChocolateFactory factory) {
	    WorkerDAO dao = (WorkerDAO) ctx.getAttribute("workerDAO");
	    Worker worker = dao.getWorkerByUsername(username);

	    if (worker != null) {
	        worker.setFactory(factory);
	        dao.updateWorker(worker);
	        return Response.status(200).build();
	    } else {
	        return Response.status(404).entity("Worker not found.").build();
	    }
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteWorker(@PathParam("id") int id) {
	    WorkerDAO dao = (WorkerDAO) ctx.getAttribute("workerDAO");
	    Worker worker = dao.getWorkerById(id);

	    if (worker != null) {
	        dao.deleteWorker(id);
	        return Response.status(200).build();
	    } else {
	        return Response.status(404).entity("Worker not found.").build();
	    }
	}
}
