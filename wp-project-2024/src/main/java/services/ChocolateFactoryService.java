package services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import dao.ChocolateFactoryDAO;
import dao.ManagerDAO;
import dao.OrderDAO;
import model.ChocolateFactory;
import model.Gender;
import model.Location;
import model.Manager;
import model.User;


@Path("factories")
public class ChocolateFactoryService {
	
	@Context
	ServletContext ctx;
	
	public ChocolateFactoryService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("chocolateFactoryDAO") == null || ctx.getAttribute("managerDAO") == null || ctx.getAttribute("orderDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("chocolateFactoryDAO", new ChocolateFactoryDAO(contextPath));
			ctx.setAttribute("managerDAO", new ManagerDAO(contextPath));
			ctx.setAttribute("orderDAO", new OrderDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ChocolateFactory> getFactories() {
		ChocolateFactoryDAO dao = (ChocolateFactoryDAO) ctx.getAttribute("chocolateFactoryDAO");
		return dao.getAllFactories();
	}
	
	@GET
	@Path("/user/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ChocolateFactory> getRateableFactories(@PathParam("name") String name, @Context HttpServletRequest request) {
        User loggedInUser = (User) request.getSession().getAttribute("user");
        
		//if(loggedInUser.getUsername().equals(name)) {
			ChocolateFactoryDAO factoryDao = (ChocolateFactoryDAO) ctx.getAttribute("chocolateFactoryDAO");
			OrderDAO orderDao = (OrderDAO) ctx.getAttribute("orderDAO");
			
			return factoryDao.getFactoriesFromOrders(orderDao.getOrdersFromUser(name));
		//}
		
		//return null;
	}
	
	@GET
	@Path("/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public ChocolateFactory getChocolateFactory(@PathParam("name") String name) {
		ChocolateFactoryDAO dao = (ChocolateFactoryDAO) ctx.getAttribute("chocolateFactoryDAO");
		return dao.getFactoryByName(name);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addChocolateFactory(@FormDataParam("logo") InputStream uploadedInputStream,
	                                    @FormDataParam("logo") FormDataContentDisposition fileDetails,
	                                    @FormDataParam("name") String name,
	                                    @FormDataParam("openingTime") String openingTime,
	                                    @FormDataParam("closingTime") String closingTime,
	                                    @FormDataParam("longitude") double longitude,
	                                    @FormDataParam("latitude") double latitude,
	                                    @FormDataParam("address") String address,
	                                    @FormDataParam("manager") int managerId,
	                                    @Context HttpServletRequest request) {
		
		if(request.getSession().getAttribute("userRole") == "ADMINISTRATOR") {

	    String uploadedFileLocation = request.getServletContext().getRealPath("/") + "images/" + fileDetails.getFileName();
	    saveLogo(uploadedInputStream, uploadedFileLocation);

	    ChocolateFactory factory = new ChocolateFactory();
	    factory.setChocolates(new ArrayList<>());
	    factory.setName(name);
	    factory.setLogo("images/" + fileDetails.getFileName());
	    factory.setLocation(new Location(longitude, latitude, address));
	    factory.setOpeningTime(LocalTime.parse(openingTime));
	    factory.setClosingTime(LocalTime.parse(closingTime));
	    factory.setStatus();

	    ChocolateFactoryDAO factoryDao = (ChocolateFactoryDAO) ctx.getAttribute("chocolateFactoryDAO");
	    factoryDao.addChocolateFactory(factory);
	    
	    ManagerDAO managerDao = (ManagerDAO) ctx.getAttribute("managerDAO");
	    Manager manager = managerDao.getManagerById(managerId);
	    manager.setFactory(factory);
	    managerDao.updateManager(manager);
		}

	    return Response.status(200).entity("Factory added successfully.").build();
	}
	
	@POST
	@Path("/new-manager")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addChocolateFactoryWithManager(@FormDataParam("logo") InputStream uploadedInputStream,
	                                    @FormDataParam("logo") FormDataContentDisposition fileDetails,
	                                    @FormDataParam("name") String name,
	                                    @FormDataParam("openingTime") String openingTime,
	                                    @FormDataParam("closingTime") String closingTime,
	                                    @FormDataParam("longitude") double longitude,
	                                    @FormDataParam("latitude") double latitude,
	                                    @FormDataParam("address") String address,
	                                    @FormDataParam("username") String username,
	                                    @FormDataParam("firstName") String firstName,
	                                    @FormDataParam("lastName") String lastName,
	                                    @FormDataParam("password") String password,
	                                    @FormDataParam("gender") Gender gender,
	                                    @FormDataParam("dateOfBirth") String dateOfBirth,
	                                    @Context HttpServletRequest request) {
		
		if(request.getSession().getAttribute("userRole") == "ADMINISTRATOR") {

	    String uploadedFileLocation = request.getServletContext().getRealPath("/") + "images/" + fileDetails.getFileName();
	    saveLogo(uploadedInputStream, uploadedFileLocation);

	    ChocolateFactory factory = new ChocolateFactory();
	    factory.setChocolates(new ArrayList<>());
	    factory.setName(name);
	    factory.setLogo("images/" + fileDetails.getFileName());
	    factory.setLocation(new Location(longitude, latitude, address));
	    factory.setOpeningTime(LocalTime.parse(openingTime));
	    factory.setClosingTime(LocalTime.parse(closingTime));
	    factory.setStatus();

	    ChocolateFactoryDAO factoryDao = (ChocolateFactoryDAO) ctx.getAttribute("chocolateFactoryDAO");
	    factoryDao.addChocolateFactory(factory);
	    
	    Manager manager = new Manager();
	    manager.setUsername(username);
	    manager.setPassword(password);
	    manager.setDateOfBirth(LocalDate.parse(dateOfBirth));
	    manager.setFirstName(firstName);
	    manager.setLastName(lastName);
	    manager.setGender(gender);
	    
	    ManagerDAO managerDao = (ManagerDAO) ctx.getAttribute("managerDAO");
	    manager.setFactory(factory);
	    managerDao.addManager(manager);
		}

	    return Response.status(200).entity("Factory added successfully with the new manager.").build();
	}

	private void saveLogo(InputStream uploadedInputStream, String uploadedFileLocation) {
	    java.nio.file.Path path = java.nio.file.Paths.get(uploadedFileLocation);
	    try (InputStream inStream = uploadedInputStream;
	            OutputStream outStream = Files.newOutputStream(path)) {
	        byte[] buffer = new byte[1024];
	        int bytesRead;
	        while ((bytesRead = uploadedInputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
}
