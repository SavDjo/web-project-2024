package services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.Administrator;
import model.Buyer;
import model.BuyerType;
import model.Manager;
import model.ShoppingCart;
import model.Order;
import model.User;
import model.Worker;
import dao.AdministratorDAO;
import dao.BuyerDAO;
import dao.ManagerDAO;
import dao.WorkerDAO;

@Path("")
public class UserService {
	
	@Context
	ServletContext ctx;
	
	public UserService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null || ctx.getAttribute("administratorDAO") == null || ctx.getAttribute("managerDAO") == null || ctx.getAttribute("workerDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("userDAO", new BuyerDAO(contextPath));
			ctx.setAttribute("administratorDAO", new AdministratorDAO(contextPath));
			ctx.setAttribute("managerDAO", new ManagerDAO(contextPath));
			ctx.setAttribute("workerDAO", new WorkerDAO(contextPath));
		}
	}
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(User user, @Context HttpServletRequest request) {
		BuyerDAO buyerDao = (BuyerDAO) ctx.getAttribute("userDAO");
		AdministratorDAO administratorDao = (AdministratorDAO) ctx.getAttribute("administratorDAO");
		if(buyerDao.buyerExists(buyerDao.getBuyerByUsername(user.getUsername())) || administratorDao.administratorExists(administratorDao.getAdministratorByUsername(user.getUsername()))) {
			return Response.status(400).entity("User is already registered").build();
		} else {
			Buyer buyer = new Buyer();
			buyer.setUsername(user.getUsername());
			buyer.setPassword(user.getPassword());
			buyer.setFirstName(user.getFirstName());
			buyer.setLastName(user.getLastName());
			buyer.setDateOfBirth(user.getDateOfBirth());
			buyer.setGender(user.getGender());
			buyer.setCart(new ShoppingCart());
			buyer.setPoints(0);
			buyer.setType(new BuyerType());
			buyer.setOrders(new ArrayList<Order>());
			buyerDao.addBuyer(buyer);
			return Response.status(200).build();
		}
	}
	
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(User user, @Context HttpServletRequest request) {
		AdministratorDAO administratorDao = (AdministratorDAO) ctx.getAttribute("administratorDAO");
    	BuyerDAO buyerDao = (BuyerDAO) ctx.getAttribute("userDAO");
    	ManagerDAO managerDao = (ManagerDAO) ctx.getAttribute("managerDAO");
    	WorkerDAO workerDao = (WorkerDAO) ctx.getAttribute("workerDAO");
    	
	    if(request.getSession().getAttribute("userRole").equals("BUYER")) {
			Buyer buyer = buyerDao.getBuyerByUsername(user.getUsername());
			buyer.setUsername(user.getUsername());
			buyer.setPassword(user.getPassword());
			buyer.setFirstName(user.getFirstName());
			buyer.setLastName(user.getLastName());
			buyer.setDateOfBirth(user.getDateOfBirth());
			buyer.setGender(user.getGender());
			buyerDao.updateBuyer(buyer);
			return Response.status(200).build();
		} else if (request.getSession().getAttribute("userRole").equals("MANAGER")) {
			Manager manager = managerDao.getManagerByUsername(user.getUsername());
			manager.setUsername(user.getUsername());
			manager.setPassword(user.getPassword());
			manager.setFirstName(user.getFirstName());
			manager.setLastName(user.getLastName());
			manager.setDateOfBirth(user.getDateOfBirth());
			manager.setGender(user.getGender());
			managerDao.updateManager(manager);
			return Response.status(200).build();
		} else if (request.getSession().getAttribute("userRole").equals("ADMINISTRATOR")) {
			Administrator administrator = administratorDao.getAdministratorByUsername(user.getUsername());
			administrator.setUsername(user.getUsername());
			administrator.setPassword(user.getPassword());
			administrator.setFirstName(user.getFirstName());
			administrator.setLastName(user.getLastName());
			administrator.setDateOfBirth(user.getDateOfBirth());
			administrator.setGender(user.getGender());
			administratorDao.updateAdministrator(administrator);
			return Response.status(200).build();
		} else {
			Worker worker = workerDao.getWorkerByUsername(user.getUsername());
			worker.setUsername(user.getUsername());
			worker.setPassword(user.getPassword());
			worker.setFirstName(user.getFirstName());
			worker.setLastName(user.getLastName());
			worker.setDateOfBirth(user.getDateOfBirth());
			worker.setGender(user.getGender());
			workerDao.updateWorker(worker);
			return Response.status(200).build();
		}
	}
	
	@GET
	@Path("/status")
	@Produces(MediaType.APPLICATION_JSON)
	public User status(@Context HttpServletRequest request) {
	    if(request.getSession().getAttribute("userRole") != null) {
	        User loggedInUser = (User) request.getSession().getAttribute("user");
	        return retrieveUser(loggedInUser);
	    } else {
	        return new User();
	    }
	}
	
	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers(@Context HttpServletRequest request) {
    	List<User> users = new ArrayList<>();
	    if(request.getSession().getAttribute("userRole").equals("ADMINISTRATOR")) {
	    	AdministratorDAO administratorDao = (AdministratorDAO) ctx.getAttribute("administratorDAO");
	    	BuyerDAO buyerDao = (BuyerDAO) ctx.getAttribute("userDAO");
	    	ManagerDAO managerDao = (ManagerDAO) ctx.getAttribute("managerDAO");
	    	WorkerDAO workerDao = (WorkerDAO) ctx.getAttribute("workerDAO");
	    	List<Buyer> buyers = new ArrayList<>(buyerDao.getAllBuyers());
	    	List<Manager> managers = new ArrayList<>(managerDao.getAllManagers());
	    	List<Worker> workers = new ArrayList<>(workerDao.getAllWorkers());
	    	List<Administrator> administrators = new ArrayList<>(administratorDao.getAllAdministrators());
	    	
	        users.addAll(buyers);
	        users.addAll(managers);
	        users.addAll(workers);
	        users.addAll(administrators);
	    }
	    
	    return users;
	}
	
	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void logout(@Context HttpServletRequest request) {
		request.getSession().invalidate();
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(User user, @Context HttpServletRequest request) {
	    BuyerDAO buyerDao = (BuyerDAO) ctx.getAttribute("userDAO");
	    AdministratorDAO administratorDao = (AdministratorDAO) ctx.getAttribute("administratorDAO");
	    ManagerDAO managerDao = (ManagerDAO) ctx.getAttribute("managerDAO");
	    WorkerDAO workerDao = (WorkerDAO) ctx.getAttribute("workerDAO");

	    Buyer loggedUser = buyerDao.getBuyerByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedUser != null) {
	        setSessionAttributes(request, loggedUser, "BUYER");
	        return Response.status(200).build();
	    }

	    Administrator loggedAdmin = administratorDao.getAdministratorByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedAdmin != null) {
	        setSessionAttributes(request, loggedAdmin, "ADMINISTRATOR");
	        return Response.status(200).build();
	    }

	    Manager loggedManager = managerDao.getManagerByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedManager != null) {
	        setSessionAttributes(request, loggedManager, "MANAGER");
	        return Response.status(200).build();
	    }
	    
	    Worker loggedWorker = workerDao.getWorkerByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedWorker != null) {
	        setSessionAttributes(request, loggedWorker, "WORKER");
	        return Response.status(200).build();
	    }

	    return Response.status(400).entity("Invalid username and/or password").build();
	}

	private void setSessionAttributes(HttpServletRequest request, User user, String userRole) {
	    request.getSession().setAttribute("user", user);
	    request.getSession().setAttribute("userRole", userRole);
	}
	
	private User retrieveUser(User user) {
	    BuyerDAO buyerDao = (BuyerDAO) ctx.getAttribute("userDAO");
	    AdministratorDAO administratorDao = (AdministratorDAO) ctx.getAttribute("administratorDAO");
	    ManagerDAO managerDao = (ManagerDAO) ctx.getAttribute("managerDAO");
	    WorkerDAO workerDao = (WorkerDAO) ctx.getAttribute("workerDAO");
		
	    Buyer loggedUser = buyerDao.getBuyerByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedUser != null) {
	        return loggedUser;
	    }

	    Administrator loggedAdmin = administratorDao.getAdministratorByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedAdmin != null) {
	        return loggedAdmin;
	    }

	    Manager loggedManager = managerDao.getManagerByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedManager != null) {
	        return loggedManager;
	    }
	    
	    Worker loggedWorker = workerDao.getWorkerByUsernameAndPassword(user.getUsername(), user.getPassword());
	    if (loggedWorker != null) {
	        return loggedWorker;
	    }
	    
	    return null;
	}

}
