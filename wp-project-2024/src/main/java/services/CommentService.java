package services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import dao.ChocolateFactoryDAO;
import dao.CommentDAO;
import dao.ManagerDAO;
import dao.OrderDAO;
import dao.WorkerDAO;
import dto.CommentDTO;
import model.ChocolateFactory;
import model.Comment;
import model.Order;
import model.OrderStatus;


@Path("comments")
public class CommentService {
	@Context
	ServletContext ctx;
	
	public CommentService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("commentDAO") == null || ctx.getAttribute("factoryDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("commentDAO", new CommentDAO(contextPath));
			ctx.setAttribute("factoryDAO", new ChocolateFactoryDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getComments() {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> existingComments = new ArrayList<>();
		for(Comment comment : dao.getAllComments()) {
			if(comment.isDeleted() == false) {
				existingComments.add(comment);
			}
		}
		return existingComments;
	}
	
	@GET
	@Path("/approved")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getApprovedComments() {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> existingComments = new ArrayList<>();
		for(Comment comment : dao.getApprovedComments()) {
			if(comment.isDeleted() == false) {
				existingComments.add(comment);
			}
		}
		return existingComments;
	}
	
	@GET
	@Path("/not-approved")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getNotApprovedComments() {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> existingComments = new ArrayList<>();
		for(Comment comment : dao.getNotApprovedComments()) {
			if(comment.isDeleted() == false) {
				existingComments.add(comment);
			}
		}
		return existingComments;
	}
	
	@GET
	@Path("/factory/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getCommentsForFactory(@PathParam("name") String name) {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		List<Comment> existingComments = new ArrayList<>();
		for(Comment comment : dao.getCommentsForFactory(name)) {
			if(comment.isDeleted() == false) {
				existingComments.add(comment);
			}
		}
		return existingComments;
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addComment(CommentDTO dto) {
	    CommentDAO commentDao = (CommentDAO) ctx.getAttribute("commentDAO");
	    ChocolateFactoryDAO factoryDao = (ChocolateFactoryDAO) ctx.getAttribute("factoryDAO");
	    Comment comment = new Comment();
	    comment.setApproved(dto.isApproved());
	    comment.setAuthor(dto.getAuthor());
	    comment.setContent(dto.getContent());
	    comment.setRating(dto.getRating());
	    comment.setFactory(dto.getFactory());


	    ChocolateFactory factory = factoryDao.getFactoryByName(dto.getFactory());
	    if(!commentDao.alreadyRated(factory.getName(), dto.getAuthor())) {
	    	commentDao.addComment(comment);
	    	double rating = commentDao.getAverageRatingForFactory(factory.getName());
	    	factory.setRating(rating);
	    	factoryDao.updateFactory(factory);
		    return Response.status(200).entity("Comment added successfully.").build();
	    }
	    
	    return Response.status(400).entity("Already rated and commented.").build();
	}
	
	
	@DELETE
	@Path("/{id}")
	public Response deleteComment(@PathParam("id") int id) {
	    CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
	    Comment comment = dao.getCommentById(id);

	    if (comment != null) {
	        dao.deleteComment(comment.getId());
	        return Response.status(200).build();
	    } else {
	        return Response.status(404).entity("Comment not found.").build();
	    }
	}
	
	@PUT
	@Path("/approve/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response approveComment(@PathParam("id") int id, @Context HttpServletRequest request) {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		
	    if(request.getSession().getAttribute("userRole") == "MANAGER") {
	    	Comment comment = dao.getCommentById(id);
	    	comment.setApproved(true);
	    	dao.updateComment(comment);
	    }
	    
	    return Response.status(200).entity("Comment updated successfully.").build();
	}
	
	@PUT
	@Path("/reject/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response rejectComment(@PathParam("id") int id, @Context HttpServletRequest request) {
		CommentDAO dao = (CommentDAO) ctx.getAttribute("commentDAO");
		
	    if(request.getSession().getAttribute("userRole") == "MANAGER") {
	    	Comment comment = dao.getCommentById(id);
	    	comment.setApproved(false);
	    	dao.updateComment(comment);
	    }
	    
	    return Response.status(200).entity("Comment updated successfully.").build();
	}
}
