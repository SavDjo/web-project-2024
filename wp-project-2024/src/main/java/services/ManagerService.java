package services;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import dao.AdministratorDAO;
import dao.BuyerDAO;
import dao.ChocolateDAO;
import dao.ChocolateFactoryDAO;
import dao.ManagerDAO;
import dao.OrderDAO;
import dao.WorkerDAO;
import model.Buyer;
import model.BuyerType;
import model.Chocolate;
import model.ChocolateCategory;
import model.ChocolateFactory;
import model.ChocolateType;
import model.Manager;
import model.Order;
import model.OrderStatus;
import model.Role;
import model.ShoppingCart;
import model.User;
import model.Worker;

@Path("managers")
public class ManagerService {
	@Context
	ServletContext ctx;
	
	public ManagerService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("managerDAO") == null || ctx.getAttribute("orderDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("managerDAO", new ManagerDAO(contextPath));
			ctx.setAttribute("orderDAO", new OrderDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Manager> getManagers() {
		ManagerDAO dao = (ManagerDAO) ctx.getAttribute("managerDAO");
		return dao.getAllManagers();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Manager getWorker(@PathParam("id") int id) {
		ManagerDAO dao = (ManagerDAO) ctx.getAttribute("managerDAO");
		return dao.getManagerById(id);
	}
	
	@GET
	@Path("/factory")
	@Produces(MediaType.APPLICATION_JSON)
	public ChocolateFactory getManagerFactory(@Context HttpServletRequest request) {
	    if(request.getSession().getAttribute("userRole") != null) {
	        Manager loggedInManager = (Manager) request.getSession().getAttribute("user");
	        if(loggedInManager == null) {
	            return null;
	        } else {
	            return loggedInManager.getFactory();
	        }
	    } else {
	        return null;
	    }
	}
	
	@GET
	@Path("/orders")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Order> getManagerOrders(@Context HttpServletRequest request) {
		OrderDAO dao = (OrderDAO) ctx.getAttribute("orderDAO");
	    if(request.getSession().getAttribute("userRole") != null) {
	        Manager loggedInManager = (Manager) request.getSession().getAttribute("user");
	        if(loggedInManager == null) {
	            return null;
	        } else {
	            return dao.getOrdersFromFactory(loggedInManager.getFactory().getName());
	        }
	    } else {
	        return null;
	    }
	}
	
	@PUT
	@Path("/orders/reject")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response rejectOrder(@FormDataParam("id") int id, @FormDataParam("rejectionReason") String rejectionReason, @Context HttpServletRequest request) {
		OrderDAO dao = (OrderDAO) ctx.getAttribute("orderDAO");
		
	    if(request.getSession().getAttribute("userRole") == "MANAGER") {
	    	Order order = dao.getOrderById(id);
	    	order.setRejectionReason(rejectionReason);
	    	order.setStatus(OrderStatus.REFUSED);
	    	dao.updateOrders(order);
	    }
	    
	    return Response.status(200).entity("Order updated successfully.").build();
	}
	
	@PUT
	@Path("/orders/approve/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response approveOrder(@PathParam("id") int id, @Context HttpServletRequest request) {
		OrderDAO dao = (OrderDAO) ctx.getAttribute("orderDAO");
		
	    if(request.getSession().getAttribute("userRole") == "MANAGER") {
	    	Order order = dao.getOrderById(id);
	    	order.setStatus(OrderStatus.APPROVED);
	    	dao.updateOrders(order);
	    }
	    
	    return Response.status(200).entity("Order updated successfully.").build();
	}
}
