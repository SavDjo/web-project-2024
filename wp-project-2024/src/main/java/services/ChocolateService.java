package services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import dao.ChocolateDAO;
import dao.ChocolateFactoryDAO;
import dao.ManagerDAO;
import dao.WorkerDAO;
import dto.ChocolateCountDTO;
import model.Chocolate;
import model.ChocolateCategory;
import model.ChocolateFactory;
import model.ChocolateStatus;
import model.ChocolateType;
import model.Location;
import model.Manager;
import model.Worker;

@Path("chocolates")
public class ChocolateService {
	@Context
	ServletContext ctx;
	
	public ChocolateService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("chocolateDAO") == null || ctx.getAttribute("workerDAO") == null || ctx.getAttribute("chocolateFactoryDAO") == null || ctx.getAttribute("managerDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("chocolateDAO", new ChocolateDAO(contextPath));
			ctx.setAttribute("chocolateFactoryDAO", new ChocolateFactoryDAO(contextPath));
			ctx.setAttribute("workerDAO", new WorkerDAO(contextPath));
			ctx.setAttribute("managerDAO", new ManagerDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Chocolate> getChocolates() {
		ChocolateDAO dao = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
		List<Chocolate> existingChocolates = new ArrayList<>();
		for(Chocolate chocolate : dao.getAllChocolates()) {
			if(chocolate.isDeleted() == false) {
				existingChocolates.add(chocolate);
			}
		}
		return existingChocolates;
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addChocolate(@FormDataParam("image") InputStream uploadedInputStream,
	                                    @FormDataParam("image") FormDataContentDisposition fileDetails,
	                                    @FormDataParam("name") String name,
	                                    @FormDataParam("description") String description,
	                                    @FormDataParam("weight") double weight,
	                                    @FormDataParam("type") ChocolateType type,
	                                    @FormDataParam("category") ChocolateCategory category,
	                                    @FormDataParam("factoryName") String factoryName,
	                                    @Context HttpServletRequest request) {

	    String uploadedFileLocation = request.getServletContext().getRealPath("/") + "images/" + fileDetails.getFileName();
	    saveImage(uploadedInputStream, uploadedFileLocation);

	    Chocolate chocolate = new Chocolate();
	    chocolate.setName(name);
	    chocolate.setWeight(weight);
	    chocolate.setDescription(description);
	    chocolate.setCount(0);
	    chocolate.setImage("images/" + fileDetails.getFileName());
	    chocolate.setType(type);
	    chocolate.setCategory(category);
	    chocolate.setStatus(ChocolateStatus.OUT_OF_STOCK);

	    ChocolateDAO dao = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
	    dao.addChocolate(chocolate, factoryName);

	    return Response.status(200).entity("Chocolate added successfully.").build();
	}
	
	private void saveImage(InputStream uploadedInputStream, String uploadedFileLocation) {
	    java.nio.file.Path path = java.nio.file.Paths.get(uploadedFileLocation);
	    try (InputStream inStream = uploadedInputStream;
	            OutputStream outStream = Files.newOutputStream(path)) {
	        byte[] buffer = new byte[1024];
	        int bytesRead;
	        while ((bytesRead = uploadedInputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	@PUT
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateChocolate(@FormDataParam("image") InputStream uploadedInputStream,
	                                    @FormDataParam("image") FormDataContentDisposition fileDetails,
	                                    @FormDataParam("name") String name,
	                                    @FormDataParam("description") String description,
	                                    @FormDataParam("weight") double weight,
	                                    @FormDataParam("type") ChocolateType type,
	                                    @FormDataParam("category") ChocolateCategory category,
	                                    @Context HttpServletRequest request) {

	    String uploadedFileLocation = "images/" + fileDetails.getFileName();
	    saveImage(uploadedInputStream, uploadedFileLocation);

	    Chocolate chocolate = new Chocolate();
	    chocolate.setName(name);
	    chocolate.setWeight(weight);
	    chocolate.setDescription(description);
	    chocolate.setType(type);
	    chocolate.setCategory(category);
	    
	    if (fileDetails.getFileName() != null && !fileDetails.getFileName().isEmpty()) {
		    chocolate.setImage(uploadedFileLocation);
	    }

	    ChocolateDAO dao = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
	    dao.updateChocolate(chocolate);

	    return Response.status(200).entity("Chocolate updated successfully.").build();
	}
	
	@PUT
	@Path("/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setChocolateCount(@PathParam("name") String name, ChocolateCountDTO dto) {
	    ChocolateDAO dao = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
	    Chocolate chocolate = dao.getChocolateByName(name);

	    if (chocolate != null) {
	        chocolate.setCount(dto.getCount());
	        if(dto.getCount() == 0) {
	        	chocolate.setStatus(ChocolateStatus.OUT_OF_STOCK);
	        } else {
	        	chocolate.setStatus(ChocolateStatus.IN_STOCK);
	        }
	        dao.updateChocolate(chocolate);
	        return Response.status(200).build();
	    } else {
	        return Response.status(404).entity("Chocolate not found.").build();
	    }
	}
	
	
	@DELETE
	@Path("/{name}")
	public Response deleteChocolate(@PathParam("name") String name) {
	    ChocolateDAO dao = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
	    Chocolate chocolate = dao.getChocolateByName(name);

	    if (chocolate != null) {
	        dao.deleteChocolate(chocolate.getId());
	        return Response.status(200).build();
	    } else {
	        return Response.status(404).entity("Chocolate not found.").build();
	    }
	}
}
